// A script to generate the Goldcoast report and mail it.

const MsSql = require("mssql");
const ExcelJs = require('exceljs');
const NodeMailer = require("NodeMailer")

const goldcoastRegular = `
begin tran

select 
    u.lms_user_id as user_id, 
    convert(varchar,  p.completed, 120) as completion_date, 
    a.manifest_id as course_id
from cs_progress p 
join cs_attempt a on p.attempt_id = a.id 
join cs_user u on u.id = a.user_id
where
    p.step = 'total'
    and p.complete = '1'
    and p.completed is not null
    and a.manifest_id != 'manifest/test-course'
    and a.manifest_id != 'manifest/testcourse_simple'
    and p.completed >= DATEADD(DAY, -7, GETDATE())
    and u.tool_consumer_instance_guid = 'gchlol.sth.health.qld.gov.au'
order by p.completed desc

rollback
`;

const goldcoastAllAttempts = `
begin tran

select
    u.lms_user_id as user_id,
    a.manifest_id as course_id,
    m.sco_id as activity_id,
    cast(json_value(m.meta, '$._score') as float) as activity_score,
    case when cast(json_value(m.meta, '$._score') as float) >= 0.75 then 'true' else 'false' end as activity_passed,
    m.created as activity_date
from cs_upload_meta_data m
join cs_attempt a on m.attempt_id = a.id
join cs_user u on u.id = a.user_id
where a.manifest_id != 'manifest/test-course'
  and a.manifest_id != 'manifest/testcourse_simple'
  and u.tool_consumer_instance_guid = 'gchlol.sth.health.qld.gov.au'
  and m.created >= '2020-01-01'
order by m.created desc

rollback
`;

function csUserFor(user) {
    return `
        begin tran

        select 
            u.lms_user_id,
            u.id
        from cs_user as u
        where lms_user_id = '${user}'

        rollback
    `;
}
function csAttemptFor(user) {
    return `
        begin tran

        select 
            a.id,
            a.manifest_id,
            a.user_id
        from cs_attempt as a
        where user_id = '${user}'

        rollback
    `;
}
function csProgressFor(attempt) {
    return `
        begin tran
        
        select
            p.completed,
            p.attempt_id,
            p.complete,
            p.step
        from cs_progress as p
        where attempt_id = '${attempt}'

        rollback
    `;
}
function csMetaFor(attempt) {
    return `
        begin tran

        select 
            m.attempt_id,
            m.sco_id,
            json_value(m.meta, '$._score') as score,
            m.created
        from cs_upload_meta_data as m
        where attempt_id = '${attempt}'

        rollback
    `;
}

function goldcoastAllAttemptsSlice(year, month) {
    const monthTo = month == 12 ? 1 : month + 1;
    const yearTo = month == 12 ? year + 1 : year;
    return `
begin tran

select
    u.lms_user_id as user_id,
    a.manifest_id as course_id,
    m.sco_id as activity_id,
    cast(json_value(m.meta, '$._score') as float) as activity_score,
    case when cast(json_value(m.meta, '$._score') as float) >= 0.75 then 'true' else 'false' end as activity_passed,
    m.created as activity_date
from cs_upload_meta_data m
join cs_attempt a on m.attempt_id = a.id
join cs_user u on u.id = a.user_id
where a.manifest_id != 'manifest/test-course'
  and a.manifest_id != 'manifest/testcourse_simple'
  and u.tool_consumer_instance_guid = 'gchlol.sth.health.qld.gov.au'
  and m.created >= '${year}-${month}-01'
  and m.created < '${yearTo}-${monthTo}-01'
order by m.created desc

rollback
`;
}


async function doSql(query) {
    try {
        const login = "datascience_ro";
        const pw = "6ReNNsGfgCmvBvrFygdG";
        const server = "hhdb0007ae.database.windows.net";
        const db = "hhcsdb0009as"
        const pool = await MsSql.connect(`mssql://${login}:${pw}@${server}/${db}?encrypt=true`);
        const result = await MsSql.query(query);
        pool.close();
        return result.recordset;
    } catch (err) {
        console.log(err);
        return null;
    }
}

function getColumnHeaders(result) {
    if (result.length > 0) {
        return Object.keys(result[0]);
    } else {
        return [];
    }
}

function getData(result, headers) {
    return result.map((record)=>headers.map((header)=>record[header]));
}

function makeWorkbook(columnHeaders, data) {
    const workbook = new ExcelJs.Workbook();
    workbook.creator = "Laerdal";
    workbook.lastModifiedBy = "Laerdal";
    workbook.created = new Date();
    workbook.modified = workbook.created;
    
    const sheet = workbook.addWorksheet("Sheet 1");

    sheet.columns = columnHeaders.map(header=>({header: header, key: header, width: 30}));
    sheet.addRows(data);
    sheet.getRow(1).font = {bold: true};
    
    return workbook;
}

async function writeWorkbook(name, workbook) {
    return workbook.xlsx.writeFile(name);
}

async function sendMail(to, title, body, attachment) {
    const transport = await NodeMailer.createTransport({
        host: 'smtp.office365.com',
        port: 587,
        secure: false,
        auth: {
            user: 'datarequest@laerdal.com',
            pass: '#udgn3WC#fBA'
        }
    });
    transport.verify(function(error, success) {
        if (error) {
            console.log(error);
        } else {
            console.log("Tranport ok");
        }
    });
    const result = await transport.sendMail({
        from: "datarequest@laerdal.com",
        to: to,
        subject: title,
        text: body,
        attachments: [
            {filename: attachment, path: attachment}
        ]
    });
}

function formatTwoDigit(v) {
    return `${v}`.padStart(2, "0");
}

/*function ordinalEnding2Digit(v) {
    return if (v == 1) "st"
    else if (v == 2) "nd"
    else if (v == 3) "rd"
    else if (v > 20) ordinalEnding2Digit(v % 10)
    else "th";
}
function ordinalEnding(v) {
    return ordinalEnding2Digit(v % 100);
}
function formatOrdinal(v) {
    return `${v}${ordinalEnding(v)}`;
}*/


async function generateAndSendReport(report, to, title, attachmentName, body) {
    const result = await doSql(report);
    const headers = getColumnHeaders(result)
    const data = getData(result, headers)
    const wb = makeWorkbook(headers, data);
    const now = new Date();
    const name = attachmentName
        .replace("{{year}}", now.getFullYear())
        .replace("{{month}}", formatTwoDigit(now.getMonth() + 1))
        .replace("{{day}}", formatTwoDigit(now.getDate()));
    console.log(`Writing ${data.length} rows to ${name}`);
    await writeWorkbook(name, wb);
    const title2 = title.replace("{{date}}", now.toLocaleDateString("en-US"));
    await sendMail(to, title2, body, name);
    console.log("done");
}

async function testSendMail() {
    await sendMail("michael.florentin@laerdal.com", "Test title", "Test body", "mine.csv")
}

//const report = goldcoastRegular;
const report = csMetaFor("76C1E4DC-E62D-EA11-A601-0003FFFEA823");
const email = "michael.florentin@laerdal.com";
const title = "{{date}} RQI data";
const body = "Hi Mellany,\n\n"
           + "The extract of today is attached below.\n\n"
           + "Kind regards,\n" 
           + "Michael Florentin\n\n";
const attachmentName = `meta274934-3.xlsx`;

// Generate and send dump.
(async()=>{
    await generateAndSendReport(
        report, 
        email, title, attachmentName, body
    ).catch(err=>console.log(err));
})();

/*(async()=>{
    const result = await doSql(report);
    const headers = getColumnHeaders(result);
    const data = getData(result, headers);
    console.log(headers);
    console.log(data[0]);
    console.log(data.length);
})();*/

