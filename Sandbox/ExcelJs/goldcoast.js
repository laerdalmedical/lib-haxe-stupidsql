// A script to generate the Goldcoast report and mail it.

const MsSql = require("mssql");
const ExcelJs = require('exceljs');
const NodeMailer = require("NodeMailer")

async function getSql() {
    try {
        const login = "datascience_ro";
        const pw = "6ReNNsGfgCmvBvrFygdG";
        const server = "hhdb0007ae.database.windows.net";
        const db = "hhcsdb0009as"
        const query = `
        begin tran

        select 
            u.lms_user_id as user_id, 
            convert(varchar,  p.completed, 120) as completion_date, 
            a.manifest_id as course_id
        from cs_progress p 
        join cs_attempt a on p.attempt_id = a.id 
        join cs_user u on u.id = a.user_id
        where
            p.step = 'total'
            and p.complete = '1'
            and p.completed is not null
            and a.manifest_id != 'manifest/test-course'
            and a.manifest_id != 'manifest/testcourse_simple'
            and p.completed >= DATEADD(DAY, -7, GETDATE())
            and u.tool_consumer_instance_guid = 'gchlol.sth.health.qld.gov.au'
        order by p.completed desc
        
        rollback
        `;
        const pool = await MsSql.connect(`mssql://${login}:${pw}@${server}/${db}?encrypt=true`);
        const result = await MsSql.query(query);
        pool.close();
        return result.recordset;
    } catch (err) {
        console.log(err);
        return null;
    }
}

function getColumnHeaders(result) {
    return [
        "user_id", 
        "completion_date", 
        "course_id"
    ];
}

function getData(result) {
    return result.map((record)=>[
        record.user_id, 
        record.completion_date, 
        record.course_id
    ]);
}

function makeWorkbook(columnHeaders, data) {
    const workbook = new ExcelJs.Workbook();
    workbook.creator = "Laerdal";
    workbook.lastModifiedBy = "Laerdal";
    workbook.created = new Date();
    workbook.modified = workbook.created;
    
    const sheet = workbook.addWorksheet("Sheet 1");

    sheet.columns = columnHeaders.map(header=>({header: header, key: header, width: 30}));
    sheet.addRows(data);
    sheet.getRow(1).font = {bold: true};
    
    return workbook;
}

async function writeWorkbook(name, workbook) {
    return workbook.xlsx.writeFile(name);
}

async function sendMail(to, title, body, attachment) {
    const transport = await NodeMailer.createTransport({
        host: 'smtp.office365.com',
        port: 587,
        secure: false,
        auth: {
            user: 'datarequest@laerdal.com',
            pass: '#udgn3WC#fBA'
        }
    });
    transport.verify(function(error, success) {
        if (error) {
            console.log(error);
        } else {
            console.log("Tranport ok");
        }
    });
    const result = await transport.sendMail({
        from: "datarequest@laerdal.com",
        to: to,
        subject: title,
        text: body,
        attachments: [
            {filename: attachment, path: attachment}
        ]
    });
}

function formatTwoDigit(v) {
    return `${v}`.padStart(2, "0");
}

/*function ordinalEnding2Digit(v) {
    return if (v == 1) "st"
    else if (v == 2) "nd"
    else if (v == 3) "rd"
    else if (v > 20) ordinalEnding2Digit(v % 10)
    else "th";
}
function ordinalEnding(v) {
    return ordinalEnding2Digit(v % 100);
}
function formatOrdinal(v) {
    return `${v}${ordinalEnding(v)}`;
}*/


async function generateAndSendReport(to, title, body) {
    const result = await getSql();
    const wb = makeWorkbook(getColumnHeaders(result), getData(result));
    const now = new Date();
    const name = `completion_report_${now.getFullYear()}_${formatTwoDigit(now.getMonth() + 1)}_${formatTwoDigit(now.getDate())}.xlsx`;
    console.log(`Writing ${name}`);
    await writeWorkbook(name, wb);
    const title2 = title.replace("{{date}}", now.toLocaleDateString("en-US"));
    await sendMail(to, title2, body, name);
    console.log("done");
}

async function testSendMail() {
    await sendMail("michael.florentin@laerdal.com", "Test title", "Test body", "mine.csv")
}

//const email = "michael.florentin@laerdal.com";
const email = "gchlol@health.qld.gov.au";
const title = "{{date}} RQI data";
const body = "Hi Mellany,\n\n"
           + "The extract of today is attached below.\n\n"
           + "Kind regards,\n" 
           + "Michael Florentin\n\n";
        
(async()=>{
    await generateAndSendReport(email, title, body).catch(err=>console.log(err));
})();

