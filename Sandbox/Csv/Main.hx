package;

using simpleutil.Env;
using simpleutil.IteratorUtils;
using simpleutil.MapUtils;
using simpleutil.Process2;
import filesystem2.NodeRTextFs;
import filesystem2.NodeWTextFs;
using stupidsql.Commands;
using stupidsql.Table;
using stupidsql.Ops;
using stupidsql.Proof;
using stupidsql.SqlQuery;
using stupidsql.TableExp;
using stupidsql.ExternalOps;

class Main {
    static public function main() {
        loadCommands();
        //sql();
        //alvin();
        //commands();
        //intl();
        //rqiAnalytics();
        //juice();
        //bug();
        //testDucks();
    }

    static private function loadCommands() {
        var args = Sys.args();
        if (args.length == 1) {
            runCommands(args[0]);
        } else {
            trace('USAGE: node test.js <file.command>');
        }
    }

    static function sql() {
        var usersString 
          = "id; name\n"
          + "u1; Michael\n"
          + "u2; Peter";
        var users = usersString.tableFromCsv("users");
        trace(users.showLines());

        var attemptsString 
          = "id; userId; activityId\n"
          + "a1; u1; adult-vent\n"
          + "a2; u1; adult-vent\n"
          + "a3; u1; adult-comp\n"
          + "a4; u2; adult-vent\n";
        var attempts = attemptsString.tableFromCsv("attempts");
        trace(attempts.showLines());

        var queryString2
            = "SELECT u.id AS userId, a.id AS attemptID, name AS userName, activityId\n"
            + "FROM users AS u JOIN attempts AS a ON u.id=a.userId\n"
            + "WHERE activityId = \"adult-comp\"";
        var query2 = queryString2.toQuery();
        var selected4 = query2.toOps().eval(
            [
                "users"=>users,
                "attempts"=>attempts
            ].fromMap(),
            ["no"=>(as)->throw"no"].get
        );
        trace(selected4.showLines());

    }

    static public function alvin() {
        runCommands("alvin.commands");
    }
    static public function commands() {
        runCommands("test.commands");
    }
    static public function intl() {
        runCommands("intl.commands");
    }

    static public function bug() {
        var commands = "bug.commands";
        var fs = NodeRTextFs.utf8();
        function print(t : Table) : Process2<{}> {
            trace(t.showLines());
            return ({}).resolved();
        }
        var process
          = fs.readFile(commands).mmap(function(content) return
            content.toCommands(commands)
            );

        process.run();
    }

    static public function rqiAnalytics() {
        runCommands("rqiAnalytics.commands");
    }

    static public function juice() {
        runCommands("juice.commands");
    }

    static private function runCommands(commands : String) : Void {
        var rfs = NodeRTextFs.utf8bom();
        var wfs = NodeWTextFs.utf8();
        var externals = [
            "no"=>(as)->throw "no"
        ].get;
        
        function print(t : Table) : Process2<{}> {
            trace(t.name + "\n" + t.proof.show() + "\n" + t.showCsvAligned());
            return ({}).resolved();
        }
        var process
          = rfs.readFile(commands).mbind(function(content) return
                content.toCommands(commands)
                    .runCommands(print, rfs, wfs, EnvUtils.empty(), externals, ExternalOps.std())
            );

        process.runTrace();
    }



    static public function testDucks() {
        var duckPoints = [6, 3, 2, 1];
        var duckNames = ["grøn", "sort", "rød", "blå"];
        //trace(ducks(7, 0, duckPoints).length);
        //trace(ducks(10, 0, duckPoints).length);
        trace(ducks(15, 0, duckPoints, duckNames).map(countDucks).showElements(function(s) return s, "\n"));
    }

    static private function ducks(
        n : Int, 
        m : Int,
        pointss : Array<Int>,
        names : Array<String>
    ) : Array<Array<String>> {
        return if (n < 0) []
        else if (n == 0) [[]]
        else {
            var result = [];
            for (type in m...pointss.length) {
                var points = pointss[type];
                var name = names[type];
                var nexts = ducks(n - points, type, pointss, names);
                for (next in nexts) {
                    result.push([name].concat(next));
                }
            }
            result;
        }
    }

    static private function countDucks(names : Array<String>) : String {
        var result = new Map();
        for (name in names) {
            result.updateTotal(name, function(n) return n + 1, 0);
        }
        return result.pairs().showElementsOxford(
            function(pair) return '${pair.value} ${pair.key}',
            ", ", " og ", " og "
        );
    }

}

