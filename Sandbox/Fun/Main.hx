package;

import simpleutil.Env;
using simpleutil.IteratorUtils;
using simpleutil.HxEscape;
using simpleutil.KeyValue;
using simpleutil.MapUtils;
using simpleutil.Process2;
using simpleutil.StringUtils;
using stringskip.StringPos;
using stupidsql.fun.Exp;
using stupidsql.fun.Type;

class Main {
    static public function main() {
        var next = 0;
        function fresh(root : String) : String {
            var id = next;
            ++next;
            return '${root}_$id';
        }
        function logError(pos : StringPos, msg : String) {
            trace('$pos : $msg');
        }
        //var e = "x_{x=7, y=\"hello\"}".stringToExp();
        var e = "{|l(x)=false, r(x)=x|} ; first^".stringToExp();

        var tenv : TypeEnvWith<StringPos> = EnvUtils.fromMap([
            "add"=>"(Int, Int)->Int".stringToType().forallAll(),
            "compose"=>"(t1->t2, t2->t3)->t1->t3".stringToType().forallAll()
        ]);
        var pair = e
            .typeLog(tenv, EnvUtils.empty(), fresh, logError);
        trace(pair.t.showIn(pair.unification));

    }



}


