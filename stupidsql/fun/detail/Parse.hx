package stupidsql.fun.detail;

using simpleutil.Annotate;
using simpleutil.CharCategories;
using simpleutil.KeyValue;
using stringskip.Parse;
using stringskip.ParseHxString;
using stringskip.Skip;
using stringskip.StringIndex;
using stringskip.StringPos;
using stupidsql.fun.Exp;
using stupidsql.fun.Type;

class Parse {
    static public function parseExp(i : StringIndex) : ExpWith<StringPos> {
        return parseExpCompose(i);
    }
    static private function parseExpCompose(i : StringIndex) : ExpWith<StringPos> {
        var e0 = parseExpApply(i);
        i.skipHxSpace();
        while (i.indicateTheChars(";") || i.indicateTheChars("?") || i.indicateTheChars("#")) {
            var pCompose = i.toPos();
            if (i.tryTheChars(";")) {
                i.skipHxSpace();
                var e = parseExpApply(i);
                i.skipHxSpace();
                e0 = ExpBase.Apply(
                    ExpBase.Var("compose").annotate(pCompose),
                    [e0, e]
                ).annotate(pCompose);
            } else if (i.tryTheChars("?")) {
                i.skipHxSpace();
                var e = parseExpApply(i);
                i.skipHxSpace();
                e0 = ExpBase.Apply(
                    ExpBase.Var("filter").annotate(pCompose),
                    [e0, e]
                ).annotate(pCompose);
            } else if (i.tryTheChars("#")) {
                i.skipHxSpace();
                var e = parseExpApply(i);
                i.skipHxSpace();
                e0 = ExpBase.Apply(
                    ExpBase.Var("order").annotate(pCompose),
                    [e0, e]
                ).annotate(pCompose);
            } else {
                throw "weird";
            }
        }
        return e0;
    }
    static private function parseExpApply(i : StringIndex) : ExpWith<StringPos> {
        var e0 = parseExpSimple(i);
        i.skipHxSpace();
        while (indicateExpSimple(i)) {
            var pApply = i.toPos();
            var as = parseExpSimples(i);
            e0 = Apply(e0, as).annotate(pApply);
            i.skipHxSpace();
        }
        return e0;
    }
    static private function parseExpSimple(i : StringIndex) : ExpWith<StringPos> {
        var i0 = i.clone();
        var es = parseExpSimples(i);
        return if (es.length == 1) es[0]
        else if (es.length > 1) {
            i0.semantic("Did not expect tuple.");
            es[0];
        } else {
            throw "weird";
        }
    }
    static private function parseExpSimples(i : StringIndex) : Array<ExpWith<StringPos>> {
        var p0 = i.toPos();
        return if (i.indicateTheChars("(")) {
            i.parseDelimitedListHx("(", ")", ",", parseExp);
        } else if (i.indicateTheChars("{|")) {
            var fs = i.parseDelimitedListHx("{|", "|}", ",", parseExpField);
            [Switch(fs).annotate(p0)];
        } else if (i.indicateTheChars("{")) {
            var fs = i.parseDelimitedListHx("{", "}", ",", parseExpField);
            [Record(fs).annotate(p0)];
        } else if (i.indicateTheChars("[")) {
            var vs = i.parseDelimitedListHx("[", "]", ",", parseExp);
            [ExpBase.List(vs).annotate(p0)];
        } else if (i.tryTheChars("~")) {
            i.skipHxSpace();
            var e = parseExpSimple(i);
            [ExpBase.Apply(
                ExpBase.Var("descending").annotate(p0),
                [e]
            ).annotate(p0)];
        } else if (i.tryTheChars("%")) {
            i.skipHxSpace();
            var xs = parseVars(i);
            i.skipHxSpace();
            i.skipTheChars(".");
            i.skipHxSpace();
            var e = parseExpSimple(i);
            [Lambda(xs, e).annotate(p0)];
        } else if (i.indicateFloat(false)) {
            var v = i.parseFloat(false);
            [ExpBase.Base(BaseValue.Float(v)).annotate(p0)];
        } else if (i.indicateInt()) {
            var v = i.parseInt();
            [ExpBase.Base(BaseValue.Int(v)).annotate(p0)];
        } else if (i.indicateTheChars("\"")) {
            var v = i.parseHxStringUtf8();
            [ExpBase.Base(BaseValue.String(v)).annotate(p0)];
        } else if (indicateVar(i)) {
            var x = parseVar(i);
            switch (x) {
                case "true": [ExpBase.Base(BaseValue.Bool(true)).annotate(p0)];
                case "false": [ExpBase.Base(BaseValue.Bool(false)).annotate(p0)];
                default:
                    i.skipHxSpace();
                    if (i.tryTheChars("_")) [GetField(x).annotate(p0)]
                    else if (i.tryTheChars("^")) [Tag(x).annotate(p0)]
                    else [ExpBase.Var(x).annotate(p0)];
            }
        } else {
            i.expected("an expression");
            i.skipToSane();
            [ExpBase.Base(BaseValue.String("?")).annotate(p0)];
        }
    }
    static private function indicateExpSimple(i : StringIndex) : Bool {
        return i.indicateTheChars("(")
            || i.indicateTheChars("[")
            || i.indicateTheChars("{")
            || i.indicateFloat(true)
            || i.indicateInt()
            || i.indicateTheChars("\"")
            || i.indicateTheChars("%")
            || indicateVar(i);
    }

    static private function parseExpField(i : StringIndex) : FieldWith<StringPos> {
        var p0 = i.toPos();
        var key = parseVar(i);
        i.skipHxSpace();
        var pAs = i.toPos();
        var as = if (indicateVars(i)) {
            var as = parseVars(i);
            i.skipHxSpace();
            as;
        } else null;
        i.skipTheChars("=");
        i.skipHxSpace();
        var value = parseExp(i);
        return if (as != null) key.mapsTo(Lambda(as, value).annotate(pAs)).annotate(p0)
        else key.mapsTo(value).annotate(p0);
    }

    static public function parseType(i : StringIndex) : TypeWith<StringPos> {
        return parseTypeArrow(i);
    }
    static private function parseTypeArrow(i : StringIndex) : TypeWith<StringPos> {
        //TODO: Allow list 
        var i0 = i.clone();
        var ts = parseTypeSimples(i);
        i.skipHxSpace();
        return if (i.tryTheChars("->")) {
            i.skipHxSpace();
            var tr = parseTypeArrow(i);
            i.skipHxSpace();
            Arrow(ts, tr).annotate(i0.toPos());
        } else {
            if (ts.length == 1) ts[0]
            else {
                i0.expected("a single type");
                TypeBase.Var("?").annotate(i0.toPos());
            }
        }
    }
    static private function parseTypeList(i : StringIndex) : TypeWith<StringPos> {
        var t0 = parseTypeSimple(i);
        i.skipHxSpace();
        var pStar = i.toPos();
        while (i.tryTheChars("*")) {
            t0 = List(t0).annotate(pStar);
            i.skipHxSpace();
            pStar = i.toPos();
        }
        return t0;
    }
    static private function parseTypeSimple(i : StringIndex) : TypeWith<StringPos> {
        var i0 = i.clone();
        var ts = parseTypeSimples(i);
        return if (ts.length == 1) ts[0]
        else {
            i0.expected("a single type");
            TypeBase.Var("?").annotate(i0.toPos());
        }
    }
    static private function parseTypeSimples(i : StringIndex) : Array<TypeWith<StringPos>> {
        var p0 = i.toPos();
        return if (i.indicateTheChars("(")) {
            i.parseDelimitedListHx("(", ")", ",", parseType);
        } else if (i.tryTheChars("{|")) {
            i.skipHxSpace();
            var row = parseTypeRow(i);
            i.skipHxSpace();
            i.skipTheChars("|}");
            [TypeBase.Sum(TypeBase.Row(row.fs, row.tl).annotate(p0)).annotate(p0)];
        } else if (i.tryTheChars("{")) {
            i.skipHxSpace();
            var row = parseTypeRow(i);
            i.skipHxSpace();
            i.skipTheChars("}");
            [TypeBase.Product(TypeBase.Row(row.fs, row.tl).annotate(p0)).annotate(p0)];
        } else if (indicateVar(i)) {
            var x = parseVar(i);
            switch (x) {
                case "Int": [TypeBase.Base(Int).annotate(p0)];
                case "Float": [TypeBase.Base(Float).annotate(p0)];
                case "Bool": [TypeBase.Base(Bool).annotate(p0)];
                case "String": [TypeBase.Base(String).annotate(p0)];
                default: [TypeBase.Var(x).annotate(p0)];
            }
        } else {
            i.expected("a type");
            i.skipToSane();
            [TypeBase.Var("?").annotate(p0)];
        }
    }

    static private function parseTypeRow(i : StringIndex) : {fs : Array<TypeFieldWith<StringPos>>, tl : Null<TypeWith<StringPos>>} {
        return if (indicateVar(i)) {
            var fs = [parseTypeField(i)];
            i.skipHxSpace();
            while (i.tryTheChars(",")) {
                i.skipHxSpace();
                fs.push(parseTypeField(i));
                i.skipHxSpace();
            }
            var tl = if (i.tryTheChars(";")) {
                i.skipHxSpace();
                parseType(i);
            } else null;
            {fs: fs, tl: tl};
        } else {fs: [], tl: null};
    }
    static private function parseTypeField(i : StringIndex) : TypeFieldWith<StringPos> {
        var p0 = i.toPos();
        var key = parseVar(i);
        i.skipHxSpace();
        i.skipTheChars(":");
        i.skipHxSpace();
        var value = parseType(i);
        return key.mapsTo(value).annotate(p0);
    }


    static private function parseVars(i : StringIndex) : Array<String> {
        return if (indicateVar(i)) [parseVar(i)]
        else if (i.indicateTheChars("(")) {
            i.parseDelimitedListHx("(", ")", ",", parseVar);
        } else {
            i.expected("a list of vars");
            i.skipToSane();
            [];
        }
    }
    static private function indicateVars(i : StringIndex) : Bool {
        return i.indicateTheChars("(") || indicateVar(i);
    }

    static private function parseVar(i : StringIndex) : String {
        return i.parseNameLike(CharCategories.isAlpha, CharCategories.isAlphanum);
    }
    static private function indicateVar(i : StringIndex) : Bool {
        return i.has() && i.get().isAlpha();
    }
}