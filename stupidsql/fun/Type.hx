package stupidsql.fun;

using Lambda;
using simpleutil.Annotate;
using simpleutil.Env;
using simpleutil.KeyValue;
using simpleutil.IteratorUtils;
using simpleutil.MapUtils;
using simpleutil.SetUtils;
using stringskip.ParserUtils;
using stringskip.StringPos;
using stupidsql.fun.detail.Parse;

typedef Type = TypeBase<Type, TypeField, BaseType>;
typedef TypeWith<TI> = Annotate<TypeBase<
    TypeWith<TI>,
    TypeFieldWith<TI>,
    BaseType
>, TI>;
enum TypeBase<TSelf, TField, TBase> {
    Var(x : String);
    Base(t : TBase);

    Product(fs : TSelf);
    Sum(fs : TSelf);
    List(t : TSelf);

    Row(fs : Array<TField>, tl : Null<TSelf>);

    Arrow(ts1 : Array<TSelf>, t2 : TSelf);
}

typedef TypeField = TypeFieldBase<Type>;
typedef TypeFieldWith<TI> = Annotate<TypeFieldBase<TypeWith<TI>>, TI>;
typedef TypeFieldBase<TType> = KeyValue<String, TType>;

enum BaseType {
    Int; Float; Bool; String;
}

typedef ForallWith<TI> = {xs : Array<String>, t : TypeWith<TI>};
typedef TypeEnvWith<TI> = Env<String, ForallWith<TI>>;

typedef UnificationWith<TI> = Env<String, TypeWith<TI>>;

class TypeUtils {
    static public function stringToType(s : String, name : String = "stringToType") : Null<TypeWith<StringPos>> {
        return s.doParseTraceNull(name, Parse.parseType.trimHxSpace());
    }

    static public function annotateConst<TI>(t : Type, info : TI) : TypeWith<TI> {
        return switch (t) {
            case Var(x): Var(x).annotate(info);
            case Base(t): Base(t).annotate(info);
            case Product(fs):
                Product(annotateConst(fs, info)).annotate(info);
            case Sum(fs):
                Sum(annotateConst(fs, info)).annotate(info);
            case List(t2):
                List(annotateConst(t2, info)).annotate(info);
            case Row(fs, tl):
                var fs2 = fs.map(function(f) return
                    f.key.mapsTo(annotateConst(f.value, info)).annotate(info)
                );
                var tl2 = if (tl != null) annotateConst(tl, info) else null;
                Row(fs2, tl2).annotate(info);
            case Arrow(t1, t2):
                Arrow(
                    t1.map(function(t) return annotateConst(t, info)),
                    annotateConst(t2, info)
                ).annotate(info);
        }
    }


    static public function eq<TI>(left : TypeWith<TI>, right : TypeWith<TI>) : Bool {
        return switch [left.value, right.value] {
            case [Var(x1), Var(x2)]: x1 == x2;
            case [Base(Int), Base(Int)]: true;
            case [Base(Float), Base(Float)]: true;
            case [Base(Bool), Base(Bool)]: true;
            case [Base(String), Base(String)]: true;
            case [Product(fs1), Product(fs2)]: eq(fs1, fs2);
            case [Sum(fs1), Sum(fs2)]: eq(fs1, fs2);
            case [List(t1), List(t2)]: eq(t1, t2);
            case [Row(fs1, tl1), Row(fs2, tl2)]:
                var eqTl = tl1 == null && tl2 == null
                        || tl1 != null && tl2 != null && eq(tl1, tl2);
                if (eqTl) {
                    var m1 = fs1.toMapString(
                        function(f) return f.value.key,
                        function(f) return f.value.value
                    );
                    var m2 = fs2.toMapString(
                        function(f) return f.value.key,
                        function(f) return f.value.value
                    );
                    m1.eqBy(m2, eq);
                } else false;
            case [Arrow(as1, r1), Arrow(as2, r2)]:
                as1.eqBy(as2, eq) && eq(r1, r2);
            default: false;
        }
    }

    static public function show<TI>(t : TypeWith<TI>) : String {
        return showIn(t, EnvUtils.empty());
    }
    static public function showIn<TI>(t : TypeWith<TI>, unification : UnificationWith<TI>) : String {
        return switch (t.v) {
            case Var(x): 
                var t2 = unification.tryGet(x);
                if (t2 != null) showIn(t2, unification)
                else x;
            case Base(Int): "Int";
            case Base(Bool): "Bool";
            case Base(Float): "Float";
            case Base(String): "String";
            case Product(t2): switch (t2.value) {
                case Row(fs, tl): '{${showRowIn(fs, tl, unification)}}';
                default: 'Product(${showIn(t2, unification)})';
            }
            case Sum(t2): switch (t2.value) {
                case Row(fs, tl): '{|${showRowIn(fs, tl, unification)}|}';
                default: 'Sum(${showIn(t2, unification)})';
            }
            case Row(fs, tl): 'Row(${showRowIn(fs, tl, unification)})';
            case List(t2): '[${showIn(t2, unification)}]';
            case Arrow(t1, t2): 
                var t1String = t1.showElements(function(t) return showIn(t, unification), ", ");
                '($t1String)->${showIn(t2, unification)}';
        }    
    }
    static private function showRowIn<TI>(
        fs : Array<TypeFieldWith<TI>>, 
        tl : Null<TypeWith<TI>>,
        unification : UnificationWith<TI>
    ) : String {
        var fsString = fs.showElements(function(f) return 
            '${f.value.key}: ${showIn(f.value.value, unification)}', ", "
        );
        var tlString = if (tl != null) '; ${showIn(tl, unification)}' else "";
        return fsString + tlString;
    }

    static public function forallAll<TI>(
        t : TypeWith<TI>
    ) : ForallWith<TI> {
        return {xs: fvs(t), t: t};
    }
    static public function fvs<TI>(
        t : TypeWith<TI>
    ) : Array<String> {
        var xs = new Set();
        function visit(t : TypeWith<TI>) : Void {
            switch (t.value) {
                case Var(x): xs.set(x, x);
                case Base(_):
                case Product(fs): visit(fs);
                case Sum(fs): visit(fs);
                case List(e): visit(e);
                case Row(fs, tl):
                    for (f in fs) {
                        visit(f.value.value);
                    }
                    if (tl != null) {
                        visit(tl);
                    }
                case Arrow(ts1, t2):
                    for (t1 in ts1) {
                        visit(t1);
                    }
                    visit(t2);
            }
        }
        visit(t);
        return xs.array();
    }
    static public function simpleSubstVar<TI>(
        t : TypeWith<TI>,
        subst : String->Null<String>
    ) : TypeWith<TI> {
        function s(t : TypeWith<TI>) : TypeWith<TI> {
            return switch (t.value) {
                case Var(x):
                    var y = subst(x);
                    if (y != null) Var(y).annotate(t.meta)
                    else t;
                case Base(_): t;
                case Product(fs): Product(s(fs)).annotate(t.meta);
                case Sum(fs): Sum(s(fs)).annotate(t.meta);
                case List(e): List(s(e)).annotate(t.meta);
                case Row(fs, tl):
                    Row(
                        fs.map(function(f) return 
                            f.value.key.mapsTo(s(f.value.value)).annotate(f.meta)
                        ),
                        if (tl != null) s(tl) else null
                    ).annotate(t.meta);
                case Arrow(ts1, t2):
                    Arrow(ts1.map(s), s(t2)).annotate(t.meta);
            }
        } 
        return s(t);
    }

    static public function unifyLog<TI>(
        t1 : TypeWith<TI>,
        t2 : TypeWith<TI>,
        unification : UnificationWith<TI>,
        fresh : String->String, 
        logError : String->Void
    ) : UnificationWith<TI> {
        function unify(t1 : TypeWith<TI>, t2 : TypeWith<TI>) : UnificationWith<TI> {
            unification = unifyLog(t1, t2, unification, fresh, logError);
            return unification;
        }
        function unifyIn(
            t1 : TypeWith<TI>, 
            t2 : TypeWith<TI>,
            unification2 : UnificationWith<TI>
        ) : UnificationWith<TI> {
            unification = unifyLog(t1, t2, unification2, fresh, logError);
            return unification;
        }
        function set(n : String, t : TypeWith<TI>) : UnificationWith<TI> {
            //TODO: Cycle check.
            unification = unification.set(n, t);
            return unification;
        }
        return switch [t1.value, t2.value] {
            case [Var(n1), Var(n2)] if (n1 == n2): unification;
            case [Var(n), t]: 
                var t3 = unification.tryGet(n);
                if (t3 != null) unify(t3, t2);
                else set(n, t2);
            case [_, Var(n)]:
                var t3 = unification.tryGet(n);
                if (t3 != null) unify(t1, t3);
                else set(n, t1);
            case [Base(Int), Base(Int)]: unification;
            case [Base(Float), Base(Float)]: unification;
            case [Base(Bool), Base(Bool)]: unification;
            case [Base(String), Base(String)]: unification;
            case [Product(r1), Product(r2)]: unify(r1, r2);
            case [Sum(r1), Sum(r2)]: unify(r1, r2);
            case [List(t1), List(t2)]: unify(t1, t2);
            case [Row(fs1, tl1), Row(fs2, tl2)]:
                var fs1Map = fs1.indexString(function(f) return f.value.key);
                var fs2Map = fs2.indexString(function(f) return f.value.key);
                var sect = fs1Map.trisect(fs2Map);
                for (key in sect.inBoth) {
                    unify(fs1Map.get(key).value.value, fs2Map.get(key).value.value);
                }
                if (tl1 == null && sect.inRight.length != 0) {
                    var vs = sect.inRight.showElementsOxfordEng(function(v) return v);
                    logError('Unmatched row fields: $vs');
                    
                }
                if (tl2 == null && sect.inLeft.length != 0) {
                    var vs = sect.inLeft.showElementsOxfordEng(function(v) return v);
                    logError('Unmatched row fields: $vs');
                }
                if (tl1 == null) {
                    if (tl2 == null) {
                        unification;
                    } else {
                        var keys1 = sect.inLeft.toSetString();
                        var fs12 = fs1.filter(function(f) return keys1.exists(f.value.key));
                        unify(Row(fs12, null).annotate(t1.meta), tl2);
                    }
                } else {
                    if (tl2 == null) {
                        var keys2 = sect.inRight.toSetString();
                        var fs22 = fs2.filter(function(f) return keys2.exists(f.value.key));
                        unify(tl1, Row(fs22, null).annotate(t2.meta));
                    } else {
                        var keys1 = sect.inLeft.toSetString();
                        var fs12 = fs1.filter(function(f) return keys1.exists(f.value.key));
                        var keys2 = sect.inRight.toSetString();
                        var fs22 = fs2.filter(function(f) return keys2.exists(f.value.key));
                        var tl3 = Var(fresh("tl")).annotate(t1.meta);
                        var unification2 = unify(Row(fs12, tl3).annotate(t1.meta), tl2);
                        unifyIn(tl1, Row(fs22, tl3).annotate(t2.meta), unification2);
                    }
                }
            case [Arrow(t11, t12), Arrow(t21, t22)]:
                if (t11.length == t21.length) {
                    for (i in 0...t11.length) {
                        unify(t11[i], t21[i]);
                    }
                    unify(t12, t22);
                } else {
                    logError('Argument count mismatch between ${show(t1)} and ${show(t2)}.');
                    unification;
                }
            default:
                logError('Cannot unify ${show(t1)} and ${show(t2)}.');
                unification;

        }
    }

    static private function showUnification<TI>(unification : UnificationWith<TI>) : String {
        return unification.showString(show);
    }
}

