package stupidsql.fun;

using simpleutil.Annotate;
using simpleutil.Env;
using simpleutil.IteratorUtils;
using simpleutil.KeyValue;
using simpleutil.MapUtils;
using stringskip.ParserUtils;
using stringskip.StringPos;
using stupidsql.fun.Type;
using stupidsql.fun.detail.Parse;

typedef Exp = ExpBase<Exp, Field<Exp>, BaseValue>;
typedef ExpWith<TI> = Annotate<ExpBase<
    ExpWith<TI>,
    FieldWith<TI>,
    BaseValue
>, TI>;
enum ExpBase<TSelf, TField, TBase> {
    Var(x : String);
    Base(v : TBase);

    Record(fs : Array<TField>); //TODO: default
    GetField(l : String);

    Tag(l : String);
    Switch(cs : Array<TField>); //TODO: default

    List(vs : Array<TSelf>);

    Lambda(xs : Array<String>, body : TSelf);
    Apply(f : TSelf, as : Array<TSelf>);
}


typedef FieldWith<TI> = Annotate<Field<ExpWith<TI>>, TI>;
typedef Field<TExp> = KeyValue<String, TExp>;

enum BaseValue {
    Int(v : Int);
    Float(v : Float);
    Bool(v : Bool);
    String(v : String);
}


typedef TypingWith<TI> = {
    t : TypeWith<TI>,
    unification : UnificationWith<TI>
}

class ExpUtils {
    static public function stringToExp(s : String, name : String = "stringToExp") : Null<ExpWith<StringPos>> {
        return s.doParseTraceNull(name, Parse.parseExp.trimHxSpace());
    }

    // Add a constant annotation on all nodes in an un-annotated expression.
    static public function annotateConst<TI>(e : Exp, info : TI) : ExpWith<TI> {
        return switch (e) {
            case Var(x): Var(x).annotate(info);
            case Base(v): Base(v).annotate(info);
            case Record(fs):
                var fs2 = fs.map(function(f) return 
                    f.key.mapsTo(annotateConst(f.value, info)).annotate(info)
                );
                Record(fs2).annotate(info);
            case GetField(l): GetField(l).annotate(info);
            case Tag(l): Tag(l).annotate(info);
            case Switch(fs):
                var fs2 = fs.map(function(f) return 
                    f.key.mapsTo(annotateConst(f.value, info)).annotate(info)
                );
                Switch(fs2).annotate(info);
            case List(es): 
                var es2 = es.map(function(e2) return annotateConst(e2, info));
                List(es2).annotate(info);
            case Lambda(x, body): Lambda(x, annotateConst(body, info)).annotate(info);
            case Apply(f, as): 
                Apply(
                    annotateConst(f, info),
                    as.map(function(a) return annotateConst(a, info))
                ).annotate(info);
        }
    }

    /*static public function tryType(e : ExpWith<StringPos>) : Null<TypeWith<StringPos>> {
        var ok = true;
        function logError(pos : StringPos, msg : String) {
            ok = false;
            trace('$pos : $msg');
        }
        var t = typeLog(e, logError);
        return if (ok) t else null;
    }*/

    // Type a an expression.
    static public function typeLog<TI>(
        e : ExpWith<TI>, 
        tenv : TypeEnvWith<TI>, 
        unification : UnificationWith<TI>, 
        fresh : String->String, 
        logError : TI->String->Void
    ) : TypingWith<TI> {
        function type(e : ExpWith<TI>) : TypeWith<TI> {
            var pair = typeLog(e, tenv, unification, fresh, logError);
            unification = pair.unification;
            return pair.t;
        }
        function typeIn(e : ExpWith<TI>, tenv : TypeEnvWith<TI>) : TypeWith<TI> {
            var pair = typeLog(e, tenv, unification, fresh, logError);
            unification = pair.unification;
            return pair.t;
        }
        function unify(t1 : TypeWith<TI>, t2 : TypeWith<TI>) : Void {
            unification = t1.unifyLog(t2, unification, fresh, function(msg) logError(e.meta, msg));
        }
        function done(t : TypeWith<TI>) : TypingWith<TI> {
            return {t: t, unification: unification};
        }
        return switch (e.value) {
            case Var(x):
                var t = tenv.get(x);
                if (t != null) {
                    var subst = t.xs
                        .map(function(x) return x.mapsTo(fresh(x)))
                        .fromPairs()
                        .toTotalFun(null);
                    done(t.t.simpleSubstVar(subst));
                } else {
                    logError(e.meta, 'Undefined variable $x');
                    done(TypeBase.Base(BaseType.String).annotate(e.meta));
                }
            case Base(v): switch (v) {
                case Int(_): done(TypeBase.Base(BaseType.Int).annotate(e.meta));
                case Float(_): done(TypeBase.Base(BaseType.Float).annotate(e.meta));
                case Bool(_): done(TypeBase.Base(BaseType.Bool).annotate(e.meta));
                case String(_): done(TypeBase.Base(BaseType.String).annotate(e.meta));
            }
            case Record(fs):
                var tfs = fs.map(function(f) return
                    f.value.key.mapsTo(type(f.value.value)).annotate(f.meta)
                );
                done(Product(
                    Row(tfs, null).annotate(e.meta)
                ).annotate(e.meta));
            case GetField(l):
                var tf : TypeWith<TI> = TypeBase.Var(fresh('${l}.FieldType')).annotate(e.meta);
                var tl : TypeWith<TI> = TypeBase.Var(fresh('${l}.TailType')).annotate(e.meta);
                done(Arrow(
                    [Product(
                        Row([l.mapsTo(tf).annotate(e.meta)], tl).annotate(e.meta)
                    ).annotate(e.meta)],
                    tf
                ).annotate(e.meta));
            case Tag(l):
                var tf : TypeWith<TI> = TypeBase.Var(fresh('${l}.TagType')).annotate(e.meta);
                var tl : TypeWith<TI> = TypeBase.Var(fresh('${l}.TailType')).annotate(e.meta);
                done(Arrow(
                    [tf],
                    Sum(
                        Row([l.mapsTo(tf).annotate(e.meta)], tl).annotate(e.meta)
                    ).annotate(e.meta)
                ).annotate(e.meta));
            case Switch(fs):
                var tr = TypeBase.Var(fresh('SwitchType')).annotate(e.meta);
                var tfs = fs.map(function(f) return {
                    var ta = TypeBase.Var(fresh('${f.value.key}.TagType')).annotate(f.meta);
                    var tExpected = Arrow([ta], tr).annotate(f.meta);
                    var tActual = type(f.value.value);
                    unify(tExpected, tActual);
                    f.value.key.mapsTo(ta).annotate(f.meta);
                });
                done(Arrow(
                    [Sum(
                        Row(tfs, null).annotate(e.meta)
                    ).annotate(e.meta)],
                    tr
                ).annotate(e.meta));
            case List(vs):
                var tElem = TypeBase.Var(fresh('ElementType')).annotate(e.meta);
                for (v in vs) {
                    var tElement = type(v);
                    unify(tElem, tElement);
                }
                done(TypeBase.List(tElem).annotate(e.meta));
            case Lambda(xs, body):
                var txs = xs.map(function(x) return {xs: [], t: TypeBase.Var(fresh('${x}ParamType')).annotate(e.meta)});
                var tenv2 = tenv.setMap(xs.zipKeyValue(txs).fromPairsString());
                var tBody = typeIn(body, tenv2);
                done(Arrow(txs.map(function(tx) return tx.t), tBody).annotate(e.meta));
            case Apply(f, as):
                var tf = type(f);
                var tas = as.map(type);
                var tr = TypeBase.Var(fresh('RetType')).annotate(e.meta);
                var tf2 = Arrow(tas, tr).annotate(e.meta);
                unify(tf2, tf);
                done(tr);
        }
    }

}
