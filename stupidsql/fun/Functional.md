Functional data queries
=======================
"Everything is functions"

The base concept is the db function, e.g.:
```
name : Int->String
```

A db function has a number of operations, the primary is application:
```
name 7
```
That would yield the name in the 7'th row. A function application can fail.

Another is search:
```
name[_=="Michael"]
```
This gives a sequence of all row indices that 

To model the relational db concept of a 

Type system
-----------
```
f : t1->t2    a : t1
--------------------
f a : t2

f : t1->t2    p : {t2}
----------------------
f[p] : t1->t2

f : t1->t2
---------------
count(f) : Int

f : t1->t2
----------------
domain(f) : [t1]

f : t1->t2
---------------
range(f) : [t2]

f : t1->t2    g : t2->t3
------------------------
f;g : t1->t3

f : t1->t2    ord : <t2>
-----------------------
f::ord : [t2]

f : t1->t2    p : {t2}
----------------------
f/p : t1->[t2]

ord : <t>
----------
~ord : <t>

ei : ti
---------------------
{li : ei} : {li : ti}

ei : ti
-----------
(ei) : (ti)

e : {li : t->ti}
----------------
!e : t->{li : ti}

e : (ti)
--------
!e : t->(ti)

l_ : {l : t; r}->t

i# : (ti; r)->ti

[t] = Int->t
{t} = t->Bool
t1=>t2 = t1->[t2]
<t> = (left : t, right : t)->(Eq | Lt | Gt | Ne)

name::ord_String; name : [String]

```

Failure
-------
