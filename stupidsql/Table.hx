package stupidsql;

using StringTools;
using simpleutil.IteratorUtils;
using stupidsql.ArrayUtils;
using stupidsql.ColumnName;
using stupidsql.Proof;
using stupidsql.Table;

typedef Table = {
    name : String, 
    proof : Proof,
    columns : Array<ColumnName>,
    data : Array<Array<String>>
}

typedef GroupedTable = {
    name : String,
    proof : Proof,
    columns : Array<ColumnName>,
    headerColumns : Array<ColumnName>,
    groups : Array<Group>
}

typedef Group = {
    header : Array<String>,
    data : Array<Array<String>>
}


class TableUtils {
    static public function indexOfColumn(
        table : Table,
        column : ColumnName
    ) : Int {
        return table.columns.indexOfColumn(column);
    }

    static public function showLines(t : Table) : String {
        return t.name + "\n"
             + t.proof.show() + "\n"
             + showLines2(t.columns, t.data);
    }
    static public function showLines2(
        cs : Array<ColumnName>, 
        data : Array<Array<String>>
    ) : String {
        var ws = cs.map(function(t) return t.showShort().length);
        for (row in data) {
            var ws1 = row.map(function(t) return t.length);
            ws = ws.zipMaxIntLong(ws1);
        }
        var s = "";
        for (i in 0...cs.length) {
            var column = cs[i];
            s += column.showShort().rpad(" ", ws[i] + 1);
        }
        s += "\n";
        for (row in data) {
            for (i in 0...row.length) {
                s += row[i].rpad(" ", ws[i] + 1);
            }
            s += "\n";
        }
        return s;
    }

    static public function showCsv(t : Table) : String {
        return
            t.columns.showElements(ColumnNameUtils.showShort, "; ") + "\n"
          + t.data.showElements(function(row) return 
                row.showElements(function(cell) return cell, "; "),
                "\n"
            );
    }
    static public function showCsvAligned(t : Table) : String {
        var ws = t.columns.map(function(t) return t.showShort().length);
        for (row in t.data) {
            var ws1 = row.map(function(t) return t.length);
            ws = ws.zipMaxIntLong(ws1);
        }
        var s = "";
        for (i in 0...t.columns.length) {
            var column = t.columns[i];
            if (i + 1 != t.columns.length) {
                s += (column.showShort() + ";").rpad(" ", ws[i] + 2);
            } else {
                s += column.showShort();
            }
        }
        s += "\n";
        for (row in t.data) {
            for (i in 0...row.length) {
                if (i + 1 != row.length) {
                    s += (row[i] + ";").rpad(" ", ws[i] + 2);
                } else {
                    s += row[i];
                }
            }
            s += "\n";
        }
        return s;
    }

    static public function showCompact(t : Table) : String {
        return "[" 
          + t.columns.showElements(ColumnNameUtils.showShort, ", ") + ": "
          + t.data.showElements((row)->
                row.showElements(ColumnNameUtils.showPart, ", "), "; "
            ) + "]";
    }

    static public function tableFromCsvGuess(s : String, name : String) : Table {
        return tableFromCsv(s, name, guessSeperator(s));
    }
    //TODO: escaping
    static public function tableFromCsv(s : String, name : String, sep : Int = ";".code) : Table {
        var i = 0;
        function atnl() : Bool {
            return i == s.length
                || s.fastCodeAt(i) == "\n".code
                || s.fastCodeAt(i) == "\r".code;
        }
        function skipnl() {
            if (i < s.length) {
                if (s.fastCodeAt(i) == "\n".code) {
                    ++i;
                    if (i < s.length && s.fastCodeAt(i) == "\r".code) {
                        ++i;
                    }
                } else if (s.fastCodeAt(i) == "\r".code) {
                    ++i;
                    if (i < s.length && s.fastCodeAt(i) == "\n".code) {
                        ++i;
                    }
                }
            }
        }
        function cell() : String {
            return if (i < s.length && s.fastCodeAt(i) == "\"".code) {
                ++i;
                var c = "";
                while (i < s.length && (s.fastCodeAt(i) != "\"".code 
                                     || i + 1 < s.length && s.fastCodeAt(i + 1) == "\"".code)) {
                    if (s.fastCodeAt(i) == "\"".code) {
                        c += "\"";
                        i += 2;
                    } else {
                        c += s.charAt(i);
                        ++i;
                    }
                }
                if (i < s.length && s.fastCodeAt(i) == "\"".code) {
                    ++i;
                }
                c;
            } else {
                var i0 = i;
                while (i < s.length && !atnl() && s.fastCodeAt(i) != sep) {
                    ++i;
                }
                s.substr(i0, i - i0).trim();
            }
        }
        function line() : Array<String> {
            var line = [];
            var first = true;
            while (!atnl()) {
                if (first) {
                    first = false;
                } else {
                    if (i < s.length && s.fastCodeAt(i) == sep) {
                        ++i;
                    }    
                }
                line.push(cell());
            }
            return line;
        }
        var cs = line().map((c)->{ns: name, name: c});
        skipnl();
        var rows = [];
        while (i < s.length) {
            rows.push(line());
            skipnl();
        }
        return {
            name: name, 
            proof: Node("csv", [name.stringProof()]),
            columns: cs,
            data: rows
        };
    }


    // Attempt to guess the cell seperator character from the first line of a 
    // csv file.
    static public function guessSeperator(s : String) : Int {
        var nComma = 0;
        var nSemicolon = 0;
        var i = 0;
        while (i < s.length && s.fastCodeAt(i) != "\r".code && s.fastCodeAt(i) != "\n".code) {
            switch (s.fastCodeAt(i)) {
                case ",".code:
                    ++nComma;
                case ";".code:
                    ++nSemicolon;
                default:
            }
            ++i;
        }
        return if (nSemicolon > nComma) ";".code
        else ",".code;
    }

}

