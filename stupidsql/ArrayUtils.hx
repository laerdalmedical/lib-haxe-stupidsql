package stupidsql;

class ArrayUtils {
    static public function zipSquashWithLong<T>(
        left : Array<T>,
        right : Array<T>,
        merge : T->T->T
    ) : Array<T> {
        var result = [];
        var l = if (left.length < right.length) left.length else right.length;
        for (i in 0...l) {
            result.push(merge(left[i], right[i]));
        }
        for (i in l...left.length) {
            result.push(left[i]);
        }
        for (i in l...right.length) {
            result.push(right[i]);
        }
        return result;
    }

    static public function zipMaxLong<T>(
        left : Array<T>,
        right : Array<T>,
        gt : T->T->Bool
    ) : Array<T> {
        return zipSquashWithLong(left, right,
            function(x, y) return if (gt(x, y)) x else y    
        );
    }

    static public function zipMaxIntLong(
        left : Array<Int>, right : Array<Int>
    ) : Array<Int> {
        return zipMaxLong(left, right, function(x, y) return x > y);
    }

    
}
