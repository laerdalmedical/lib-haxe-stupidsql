package stupidsql;

using Lambda;
using StringTools;
using simpleutil.CharCategories;
using simpleutil.MapUtils;
using simpleutil.StringUtils;
using stringskip.StringTemplate;

class ExternalOps {
    static public function std() : String->Null<Array<String>->String> {
        return [
            "alphanumOnly"=>alphanumOnlyOp,
            "lower"=>lowerOp,
            "match"=>matchOp,
            "removeSpace"=>removeSpaceOp,
            "replace"=>replaceOp
        ].toTotalFun(null);
    }

    static public function alphanumOnlyOp(as : Array<String>) : String {
        return if (as.length == 1) {
            as[0].codes().filter(CharCategories.isAlphanum).stringFromCodes();
        } else throw "lowerOp: expected one argument";
    }
    static public function lowerOp(as : Array<String>) : String {
        return if (as.length == 1) as[0].toLowerCase()
        else throw "lowerOp: expected one argument";
    }
    static public function matchOp(as : Array<String>) : String {
        return if (as.length == 2) {
            var map = as[1].match(as[0]);
            if (map.valid) "true" else "false";
        } else throw 'matchOp: expected two arguments';
    }
    static public function removeSpaceOp(as : Array<String>) : String {
        return if (as.length == 1) as[0].replace(" ", "")
        else throw "removeSpaceOp: expected one argument";
    }
    static public function replaceOp(as : Array<String>) : String {
        return if (as.length == 3) {
            var map = as[1].matchTrace(as[0]);
            if (map.valid) {
                as[2].expandSimple(map.value.toTotalFun(""));
            } else throw "replaceOp: didn't match";
        } else throw "replaceOp: expected three arguments";
    }

}