package stupidsql;

using Lambda;
using Std;
using simpleutil.HxEscape;
using simpleutil.IteratorUtils;
using simpleutil.StringTokenCompare;
using simpleutil.StringUtils;
using stringskip.Parse;
using stringskip.ParserUtils;
using stringskip.Skip;
using stupidsql.ColumnName;
using stupidsql.DateTime;
using stupidsql.Table;
using stupidsql.ValueExp;


enum ValueExp {
    Row(cs : Array<ValueExp>);
    All;

    As(e : ValueExp, name : ColumnName);

    Column(name : ColumnName);
    Literal(value : String);
    Eq(e1 : ValueExp, e2 : ValueExp);
    Lt(e1 : ValueExp, e2 : ValueExp);
    And(e1 : ValueExp, e2 : ValueExp);
    Or(e1 : ValueExp, e2 : ValueExp);
    Not(e2 : ValueExp);
    Case(cs : Array<{cond : ValueExp, value : ValueExp}>, fallback : ValueExp);
    Extract(part : DateTimePart, dt : ValueExp);
    Parse(s : ValueExp, type : ParseType, culture : Null<Culture>);
    StringPrefix(s : ValueExp, prefix : String);
    StringPostfix(s : ValueExp, postfix : String);
    StringContains(s : ValueExp, substring : String);

    Count(e : ValueExp);

    Distinct(e : ValueExp);

    AggregateWindowed(agg : ValueExp, partition : ValueExp, order : Null<ValueExp>);
    AggregateWindowedPrepared(agg : ValueExp, partition : ValueExp, order : Null<ValueExp>, groups : Map<String, Group>);

    Op(op : String, as : Array<ValueExp>);
}


enum ParseType {
    Date;
    //TODO: more
}
enum Culture {
    EnUS;
    //TODO: more
}

enum DateTimePart {
    Year; Month; Day; 
    Hour; Minute; Second;
    Quarter;
}

enum ValueType {
    String;
    DateTime;
    Number;
    Bool;
    Null;
}

class ValueExpUtils {

    static public function isAggregate(e : ValueExp) : Bool {
        return isAggWorker(e, function(e2) return switch (e2) {
            case Count(_): true;
            default: false;
        });
    }

    static private function isAggWorker(e : ValueExp, isLeaf : ValueExp->Bool) : Bool {
        function isAgg(e : ValueExp) : Bool {
            return isAggWorker(e, isLeaf);
        }
        return switch (e) {
            case Row(cs): cs.exists(isAgg);
            case As(e2, _): isAgg(e2);
            case Eq(e1, e2): isAgg(e1) || isAgg(e2);
            case Lt(e1, e2): isAgg(e1) || isAgg(e2);
            case And(e1, e2): isAgg(e1) || isAgg(e2);
            case Or(e1, e2): isAgg(e1) || isAgg(e2);
            case Not(e2): isAgg(e2);
            case Case(cs, fallback): 
                cs.exists(function(c) return 
                    isAgg(c.cond) || isAgg(c.value)
                ) || isAgg(fallback);
            case Extract(_, e2): isAgg(e2);
            case StringPrefix(s, _): isAgg(s);
            case StringPostfix(s, _): isAgg(s);
            case StringContains(s, _): isAgg(s);
            case Op(_, as): as.exists(isAgg);
            default: isLeaf(e);
        }
    }

    static public function show(e : ValueExp) : String {
        return switch (e) {
            case Row(cs): "row(" + cs.showElements(show, ", ") + ")";
            case All: '*';
            case As(e2, name): '(${show(e2)} AS ${name.show()})';
            case Column(x): x.show();
            case Literal(v): "'" + v.hxEscapeChars() + "'";
            case Eq(e1, e2): '(${show(e1)} = ${show(e2)})';
            case Lt(e1, e2): '(${show(e1)} < ${show(e2)})';
            case And(e1, e2): '(${show(e1)} AND ${show(e2)})';
            case Or(e1, e2): '(${show(e1)} OR ${show(e2)})';
            case Not(e2): '(NOT ${show(e2)})';
            case Case(cs, f):
                var csString = cs.showElements(function(c) return 
                    'WHEN ${show(c.cond)} THEN ${show(c.value)}', " "
                );
                'CASE $csString ELSE ${show(f)} END';
            case Extract(part, e2):
                var partString = switch (part) {
                    case Year: "YEAR";
                    case Month: "MONTH";
                    case Day: "DAY";
                    case Hour: "HOUR";
                    case Minute: "MINUTE";
                    case Second: "SECOND";
                    case Quarter: "QUARTER";
                }
                'EXTRACT($partString, ${show(e2)})';
            case Parse(s, type, culture):
                var typeString = switch (type) {
                    case Date: "date";
                }
                var cultureString = if (culture != null) switch (culture) {
                    case EnUS: " USING en-US";
                } else "";
                'PARSE(${show(s)} AS $typeString$cultureString)';
            case StringPrefix(s, prefix): 
                'stringPrefix(${show(s)}, ${prefix.hxEscape()})';
            case StringPostfix(s, postfix):
                'stringPostfix(${show(s)}, ${postfix.hxEscape()})';
            case StringContains(s, substring):
                'stringContains(${show(s)}, ${substring.hxEscape()})';
            case Count(e2): 'count(${show(e2)})';
            case Distinct(e2): 'distinct(${show(e2)})';
            case AggregateWindowed(agg, partition, order):
                var orderString = if (order != null) order.show() else "-";
                'aggregateWindowed(${agg.show()}, ${partition.show()}, $orderString)';
            case AggregateWindowedPrepared(agg, partition, order, groups):
                var orderString = if (order != null) order.show() else "-";
                'aggregateWindowed(${agg.show()}, ${partition.show()}, $orderString)';
            case Op(_, _): "«" + showExternal(e) + "»";
        }
    }

    static public function showExternal(e : ValueExp) : String {
        return switch (e) {
            case Op(op, as): '$op(${as.showElements(showExternal, ", ")})';
            case Literal(v): "'" + v.hxEscapeChars() + "'";
            case Column(x): x.show();
            default: "›" + show(e) + "‹";
        }
    }

    static public function getColumnName(
        e : ValueExp,
        columns : Array<ColumnName>
    ) : ColumnName {
        return switch (e) {
            case Row(_): throw "not name";
            case All: throw "not name";
            case As(_, name): name;
            case Column(name): name;
            case Literal(v): {ns: null, name: v};
            case Eq(_, _): {ns: null, name: "eq"};
            case Lt(_, _): {ns: null, name: "lt"};
            case And(_, _): {ns: null, name: "and"};
            case Not(_): {ns: null, name: "not"};
            case Or(_, _): {ns: null, name: "or"};
            case Case(_, _): {ns: null, name: "case"};
            case Extract(_, _): {ns: null, name: "extract"};
            case Parse(_, _, _): {ns: null, name: "parse"};
            case StringPrefix(_, _): {ns: null, name: "prefix"};
            case StringPostfix(_, _): {ns: null, name: "postfix"};
            case StringContains(_, _): {ns: null, name: "contains"};
            case Count(_): {ns: null, name: "count"};
            case Distinct(_): {ns: null, name: "distinct"};
            case AggregateWindowed(_, _, _): {ns: null, name: "agg"};
            case AggregateWindowedPrepared(_, _, _, _): {ns: null, name: "agg"};
            case Op(op, _): {ns: null, name: 'op$op'};
        }
    }

    static public function getColumnNames(
        e : ValueExp,
        columns : Array<ColumnName>
    ) : Array<ColumnName> {
        return switch (e) {
            case Row(cs): cs.mapconcat(function(c) return getColumnNames(c, columns)).array();
            case Distinct(c): getColumnNames(c, columns);
            case All: columns;
            default: [getColumnName(e, columns)];
        }
    }

    //TODO: consider refactoring the evala to return a value type.
    static public function evalAsValue(
        e : ValueExp, 
        columns : Array<ColumnName>, 
        row : Array<String>,
        externals : String->Null<Array<String>->String>
    ) : String {
        function eval(e : ValueExp) : String {
            return evalAsValue(e, columns, row, externals);
        }
        return switch (e) {
            case Row(_): throw "row not a value";
            case All: throw "all not a value";
            case As(e2, _): eval(e2);
            case Column(x):
                var i = columns.indexOfColumn(x);
                if (0 <= i) row[i]
                else {
                    throw 'evalAsValue: No such column ${x.show()}';
                }
            case Literal(v): v;
            case Eq(e1, e2):
                if (eval(e1) == eval(e2)) "true"
                else "false";
            case Lt(e1, e2):
                var v1 = eval(e1);
                var v2 = eval(e2);
                var c = if (v1.indicateDateTime() && v2.indicateDateTime()) 
                    v1.asDateTime().lt(v2.asDateTime())
                else if (v1.indicateNumber() && v2.indicateNumber())
                    v1.asNumber() < v2.asNumber()
                else throw "can only compare numbers or date-times";
                if (c) "true" else "false";
            case And(e1, e2):
                if (eval(e1).asBool() && eval(e2).asBool()) "true"
                else "false";
            case Or(e1, e2):
                if (eval(e1).asBool() || eval(e2).asBool()) "true"
                else "false";
            case Not(e2): 
                if (eval(e2).asBool()) "false"
                else "true";
            case Case(cs, f):
                for (c in cs) {
                    if (eval(c.cond).asBool()) {
                        return eval(c.value);
                    }
                }
                eval(f);
            case Extract(part, e2):
                var dt = eval(e2).asDateTime();
                switch (part) {
                    case Year: 
                        dt.date.year.string();
                    case Month: 
                        dt.date.month.string();
                    case Day: 
                        dt.date.day.string();
                    case Hour: 
                        if (dt.time != null) dt.time.hour.string()
                        else "0";
                    case Minute: 
                        if (dt.time != null) dt.time.minute.string()
                        else "0";
                    case Second:
                        if (dt.time != null) dt.time.second.string()
                        else "0";
                    case Quarter:
                        if (1 <= dt.date.month && dt.date.month <= 3) "1"
                        else if (dt.date.month <= 6) "2"
                        else if (dt.date.month <= 9) "3"
                        else if (dt.date.month <= 12) "4"
                        else "0";
                }
            case Parse(s, type, culture):
                var v = eval(s);
                switch (type) {
                    case Date: if (culture != null) switch (culture) {
                        case EnUS: 
                            var dt = v.toDateTimeEnUS();
                            if (dt != null) dt.show() else "null";
                    } else {
                        var dt = v.toDateTime();
                        if (dt != null) dt.show() else "null";
                    }
                }
            case StringPrefix(s, prefix):
                var string = eval(s);
                if (string.startsWith(prefix)) "true" else "false";
            case StringPostfix(s, postfix):
                var string = eval(s);
                if (string.endsWith(postfix)) "true" else "false";
            case StringContains(s, substring):
                var string = eval(s);
                if (string.indexOf(substring) >= 0) "true" else "false";
            case Count(_): throw "count not a value";
            case Distinct(_): throw "distint must be prepared.";
            case AggregateWindowed(_, _, _):
                throw "aggregateWindows must be prepared.";
            case AggregateWindowedPrepared(agg, partition, order, groups):
                throw "TODO";
            case Op(op, as):
                var f = externals(op);
                if (f != null) {
                    var vs = as.map(eval);
                    f(vs);
                } else throw 'undefined op $op';
        }
    }
    
    static public function evalAsRow(
        e : ValueExp,
        columns : Array<ColumnName>,
        row : Array<String>,
        externals : String->Null<Array<String>->String>
    ) : Array<String> {
        return switch (e) {
            case Row(cs): 
                cs.mapconcat(function(c) return evalAsRow(c, columns, row, externals)).array();
            case All: row;
            default: [evalAsValue(e, columns, row, externals)];
        }
    }

    static public function evalAsAggregateValue(
        e : ValueExp,
        columns : Array<ColumnName>,
        headerColumns : Array<ColumnName>,
        group : Group,
        externals : String->Null<Array<String>->String>
    ) : String {
        function eval(e : ValueExp) : String {
            return evalAsAggregateValue(e, columns, headerColumns, group, externals);
        }
        return switch (e) {
            case As(e2, _): eval(e2);
            case Column(name):  
                var iColumn = headerColumns.indexOfColumn(name);
                if (iColumn < 0) {
                    throw 'evalAsAggregateValue: No such column ${name.show()}';
                }
                group.header[iColumn];
            case Literal(v): v;
            case Eq(e1, e2): 
                if (eval(e1) == eval(e2)) "true"
                else "false";
            case Lt(e1, e2):
                var v1 = eval(e1);
                var v2 = eval(e2);
                var c 
                  = if (v1.indicateDateTime() && v2.indicateDateTime()) 
                        v1.asDateTime().lt(v2.asDateTime())
                    else if (v1.indicateNumber() && v2.indicateNumber())
                        v1.asNumber() < v2.asNumber()
                    else throw "can only compare numbers or date-times";
                if (c) "true" else "false";
            case And(e1, e2):
                if (eval(e1).asBool() && eval(e2).asBool()) "true"
                else "false";
            case Or(e1, e2):
                if (eval(e1).asBool() || eval(e2).asBool()) "true"
                else "false";
            case Not(e2): 
                if (eval(e2).asBool()) "false" else "true";
            case Case(cs, f):
                for (c in cs) {
                    if (eval(c.cond).asBool()) {
                        return eval(c.value);
                    }
                }
                eval(f);
            case StringPrefix(s, prefix):
                var string = eval(s);
                if (string.startsWith(prefix)) "true" else "false";
            case StringPostfix(s, postfix):
                var string = eval(s);
                if (string.endsWith(postfix)) "true" else "false";
            case StringContains(s, substring):
                var string = eval(s);
                if (string.indexOf(substring) >= 0) "true" else "false";
                case Count(All): '${group.data.length}';
            case Count(e2):
                var n = 0;
                for (row in group.data) {
                    var v = evalAsValue(e2, columns, row, externals);
                    if (v != "null") {
                        ++n;
                    }
                }
                '$n';
            case Op(op, as):
                var f = externals(op);
                if (f != null) {
                    var vs = as.map(eval);
                    f(vs);
                } else throw 'undefined op $op';
            default: throw "not aggregate";
        }
    }

    static public function evalAsAggregateRow(
        e : ValueExp,
        columns : Array<ColumnName>,
        headerColumns : Array<ColumnName>, 
        group : Group,
        externals : String->Null<Array<String>->String>
    ) : Array<String> {
        return switch (e) {
            case Row(cs): cs.map(function(c) return evalAsAggregateValue(c, columns, headerColumns, group, externals));
            default: [evalAsAggregateValue(e, columns, headerColumns, group, externals)];
        }
    }

    static public function asBool(s : String) : Bool {
        return switch (s) {
            case "true": true;
            case "false": false;
            default:
                throw "not a boolean";
        }
    }
    static public function indicateBool(s : String) : Bool {
        return s == "true" || s == "false";
    }

    static public function asDateTime(s : String) : DateTime {
        var v = s.toDateTime(s);
        return if (v != null) v
        else throw "not a DateTime";
    }
    static public function indicateDateTime(s : String) : Bool {
        return DateUtils.indicateDate(s);
    }

    static public function asNumber(s : String) : Float {
        var v = s.doParseTraceNull("number", function(i) return i.parseFloat(true));
        return if (v != null) v
        else throw "not a number";
    }
    static public function indicateNumber(s : String) : Bool {
        return s.doIndicate("number", function(i) return i.indicateFloat(true));
    }

    static public function typeOfValue(s : String) : ValueType {
        return if (s == "null") Null
        else if (indicateBool(s)) Bool
        else if (indicateDateTime(s)) DateTime
        else if (indicateNumber(s)) Number
        else String;
    }
    static public function lubType(t1 : ValueType, t2 : ValueType) : ValueType {
        return switch [t1, t2] {
            case [Null, Null]: Null;
            case [Null, t]: t;
            case [t, Null]: t;
            case [Bool, Bool]: Bool;
            case [DateTime, DateTime]: DateTime;
            case [Number, Number]: Number;
            default: String;
        }
    }
    static public function orderTyped(t : ValueType) : String->String->Int {
        return switch (t) {
            case Null: function(_, _) return 0;
            case Bool: orderBool;
            case DateTime: orderDateTime;
            case Number: orderNumber;
            case String: orderString;
        }
    }

    static public function orderBool(s1 : String, s2 : String) : Int {
        return orderNull(s1, s2, function(s1, s2) return switch [s1, s2] {
            case ["false", "false"]: 0;
            case ["false", "true"]: -1;
            case ["true", "false"]: 1;
            case ["true", "true"]: 0;
            default: throw "not bool";
        });
    }
    static public function orderDateTime(s1 : String, s2 : String) : Int {
        return orderNull(s1, s2, function(s1, s2) return {
            var dt1 = asDateTime(s1);
            var dt2 = asDateTime(s2);
            if (dt1.eq(dt2)) 0
            else if (dt1.lt(dt2)) -1
            else 1;
        });
    }
    static public function orderNumber(s1 : String, s2 : String) : Int {
        return orderNull(s1, s2, function(s1, s2) return {
            var v1 = asNumber(s1);
            var v2 = asNumber(s2);
            if (v1 < v2) -1
            else if (v1 > v2) 1
            else 0;
        });
    }
    static public function orderString(s1 : String, s2 : String) : Int {
        return orderNull(s1, s2, function(s1, s2) return
            s1.toLowerCase().stringTokenCompare(s2.toLowerCase())
        );
    }

    static private function orderNull(s1 : String, s2 : String, order : String->String->Int) : Int {
        return switch [s1, s2] {
            case ["null", "null"]: 0;
            case ["null", _]: -1;
            case [_, "null"]: 1;
            default: order(s1, s2);
        }
    }

}



