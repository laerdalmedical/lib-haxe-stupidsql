package stupidsql;

using simpleutil.HxEscape;
using simpleutil.IteratorUtils;

typedef ColumnName = {
    ns : Null<String>,
    name : String
}

class ColumnNameUtils {
    static public function column(name : String) : ColumnName {
        return {ns: null, name: name};
    }

    // Index of the column given by `column`. 
    // <0 if the column was not found.
    static public function indexOfColumn(
        columns : Array<ColumnName>,
        column : ColumnName
    ) : Int {
        return columns.indexThat(isColumnC(column));
    }

    static public function renameNs(column : ColumnName, ns : String) : ColumnName {
        return {ns: ns, name: column.name};
    }
    static public function renameNss(cs : Array<ColumnName>, ns : String) : Array<ColumnName> {
        return cs.map((c)->renameNs(c, ns));
    }

    // True iff the column name `requested` refers to `actual`.
    // E.g.
    //   isColumn(⟦users.name⟧, ⟦users.name⟧)
    //   isColumn(⟦name⟧, ⟦users.name⟧)
    static public function isColumn(
        requested : ColumnName,
        actual : ColumnName
    ) : Bool {
        return requested.ns != null && actual.ns != null && requested.ns == actual.ns && requested.name == actual.name
            || requested.ns == null && requested.name == actual.name;
    }
    static public function isColumnC(
        requested : ColumnName
    ) : ColumnName->Bool {
        return (actual)->isColumn(requested, actual);
    }

    static public function eq(c1 : ColumnName, c2 : ColumnName) : Bool {
        return c1.ns == c2.ns 
            && c1.name == c2.name;
    }
    static public function eqs(cs1 : Iterable<ColumnName>, cs2 : Iterable<ColumnName>) : Bool {
        return cs1.eqBy(cs2, eq);
    }

    static public function eqName(c1 : ColumnName, c2 : ColumnName) : Bool {
        return c1.name == c2.name;
    }
    static public function eqNames(cs1 : Iterable<ColumnName>, cs2 : Iterable<ColumnName>) : Bool {
        return cs1.eqBy(cs2, eqName);
    }

    //TODO: escape
    static public function show(n : ColumnName) : String {
        return if (n.ns != null) '${showPart(n.ns)}.${showPart(n.name)}' 
        else showPart(n.name);
    }
    static public function shows(ns : Iterable<ColumnName>) : String {
        return ns.showElements(show, ", ");
    }
    static public function showShort(n : ColumnName) : String {
        return showPart(n.name);
    }

    static public function showPart(s : String) : String {
        return s.hxEscapeIfNonAlphanum();
    }
}
