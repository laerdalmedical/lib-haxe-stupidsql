package stupidsql;

using simpleutil.HxEscape; 
using stupidsql.DateTime;

enum Value {
    Null;
    String(v : String);
    Int(v : Int);
    Float(v : Int);
    Bool(v : Bool);
    DateTime(v : DateTime);
}

class ValueUtils {
    static public function show(v : Value) : String {
        return switch (v) {
            case Null: "null";
            case String(v): "'" + v.hxEscapeChars() + "'";
            case Int(v): '$v';
            case Float(v): '$v';
            case Bool(v): if (v) "true" else "false";
            case DateTime(v): v.show();
        }
    }
}