package stupidsql;

using Std;
using StringTools;
using stringskip.ParserUtils;
using stupidsql.DateTime;
using stupidsql.detail.ParseDateTime;

typedef Date = {
    year : Int,
    month : Int,
    day : Int
}
typedef Time = {
    hour : Int,
    minute : Int,
    second : Int
}
typedef DateTime = {
    date : Date,
    time : Null<Time>
}

class DateUtils {

    static public function indicateDate(s : String, name : String = "datetime") : Bool {
        return s.doIndicate(name, ParseDateTime.indicateDate);
    }

    static public function show(d : Date) : String {
        return '${d.year.string().lpad("0", 4)}-${d.month.string().lpad("0", 2)}-${d.day.string().lpad("0", 2)}';
    }
    static public function eq(d1 : Date, d2 : Date) : Bool {
        return d1.year == d2.year
            && d1.month == d2.month
            && d1.day == d2.day;
    }
    static public function lt(d1 : Date, d2 : Date) : Bool {
        return d1.year < d2.year
            || (d1.year == d2.year
             && d1.month < d2.month
             || (d1.month == d2.month
              && d1.day < d2.day  
                )   
               );
    }
}
class TimeUtils {
    static public function show(t : Time) : String {
        return '${t.hour.string().lpad("0", 2)}:${t.minute.string().lpad("0", 2)}:${t.second.string().lpad("0", 2)}';
    }
    static public function eq(t1 : Time, t2 : Time) : Bool {
        return t1.hour == t2.hour
            && t1.minute == t2.minute
            && t1.second == t2.second;
    }
    static public function lt(t1 : Time, t2 : Time) : Bool {
        return t1.hour < t2.hour
            || (t1.hour == t2.hour
             && t1.minute < t2.minute
             || (t1.minute == t2.minute
              && t1.second < t2.second  
                )   
               );
    }
}
class DateTimeUtils {
    static public function toDateTime(s : String, name : String = "datetime") : Null<DateTime> {
        return s.doParseTraceNull(name, ParseDateTime.parseDateTime);
    }
    static public function toDateTimeEnUS(s : String, name : String = "datetime") : Null<DateTime> {
        return s.doParseTraceNull(name, ParseDateTime.parseDateTimeEnUS);
    }

    static public function show(dt : DateTime) : String {
        return if (dt.time != null) '${dt.date.show()} ${dt.time.show()}'
        else dt.date.show();
    }
    static public function eq(dt1 : DateTime, dt2 : DateTime) : Bool {
        return dt1.time != null && dt2.time != null && dt1.date.eq(dt2.date) && dt1.time.eq(dt2.time)
            || dt1.time == null && dt2.time == null && dt1.date.eq(dt2.date);
    }
    static public function lt(dt1 : DateTime, dt2 : DateTime) : Bool {
        return dt1.date.lt(dt2.date)
            || (dt1.date.eq(dt2.date)
             && dt1.time == null && dt2.time != null
             || (dt1.time != null && dt2.time != null
              && dt1.time.lt(dt2.time)  
                )
               );
    }

}