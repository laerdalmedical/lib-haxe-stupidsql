package stupidsql;

using simpleutil.Env;
using simpleutil.HxEscape;
using simpleutil.IteratorUtils;
using stupidsql.ColumnName;
using stupidsql.Ops;
using stupidsql.Table;
using stupidsql.TableExp;
using stupidsql.ValueExp;

enum TableExp {
    Var(x : String);
    RenameTable(t : TableExp, name : String);
    JoinEq(left : TableExp, right : TableExp, leftColumn : ColumnName, rightColumn : ColumnName);
    JoinLeftEq(left : TableExp, right : TableExp, leftColumn : ColumnName, rightColumn : ColumnName);
    JoinRightEq(left : TableExp, right : TableExp, leftColumn : ColumnName, rightColumn : ColumnName);
    JoinOuterEq(left : TableExp, right : TableExp, leftColumn : ColumnName, rightColumn : ColumnName);
    Product(left : TableExp, right : TableExp);
    Union(left : TableExp, right : TableExp);
    //TODO: ProductLeft and ProductRight
    SelectColumns(t : TableExp, columns : Array<ColumnName>);
    RenameColumn(t : TableExp, from : ColumnName, to : ColumnName);
    Filter(t : TableExp, p : ValueExp);
    WithColumn(t : TableExp, name : ColumnName, value : ValueExp);
    Distinct(t : TableExp, value : ValueExp);
    Map(t : TableExp, e : ValueExp);
    GroupBy(t : TableExp, e : ValueExp);
    Aggregate(t : TableExp, e : ValueExp);
    AggregateAll(t : TableExp, e : ValueExp);
    Sort(t : TableExp, e : ValueExp, asc : Bool);
}

class TableExpUtils {

    static public function show(e : TableExp) : String {
        return switch (e) {
            case Var(x): x;
            case RenameTable(t, name):
                'rename(${show(t)}, $name)';
            case JoinEq(left, right, leftColumn, rightColumn):
                'joinEq(${show(left)}, ${show(right)}, ${leftColumn.show()}, ${rightColumn.show()})';
            case JoinLeftEq(left, right, leftColumn, rightColumn):
                'joinLeftEq(${show(left)}, ${show(right)}, ${leftColumn.show()}, ${rightColumn.show()})';
            case JoinRightEq(left, right, leftColumn, rightColumn):
                'joinRightEq(${show(left)}, ${show(right)}, ${leftColumn.show()}, ${rightColumn.show()})';
            case JoinOuterEq(left, right, leftColumn, rightColumn):
                'joinOuterEq(${show(left)}, ${show(right)}, ${leftColumn.show()}, ${rightColumn.show()})';
            case Product(left, right):
                'product(${show(left)}, ${show(right)})';
            case Union(left, right):
                'union(${show(left)}, ${show(right)})';
            case SelectColumns(t, columns):
                'columns(${show(t)}, [${columns.showElements(ColumnNameUtils.show, ", ")}])';
            case RenameColumn(t, from, to):
                'as(${show(t)}, ${from.show()}, ${to.show()})';
            case Filter(t, p):
                'filter(${show(t)}, ${p.show()})';
            case WithColumn(t, name, e):
                'with(${show(t)}, ${name.show()}, ${e.show()})';
            case Distinct(t, e):
                'distinct(${show(t)}, ${e.show()})';
            case Map(t, e):
                'map(${show(t)}, ${e.show()})';
            case GroupBy(t, e):
                'groupBy(${show(t)}, ${e.show()})';
            case Aggregate(t, e):
                'aggregate(${show(t)}, ${e.show()})';
            case AggregateAll(t, e):
                'aggregateAll(${show(t)}, ${e.show()})';
            case Sort(t, e, asc):
                'sort(${show(t)}, ${e.show()}), ${if (asc) "true" else "false"}';
                    
        }
    }

    static public function eval(
        e : TableExp, 
        env : Env<String, Table>,
        externals : String->Null<Array<String>->String>
    ) : Table {
        function recurse(e : TableExp) : Table {
            return eval(e, env, externals);
        }
        return switch (e) {
            case Var(x):
                var t = env.tryGet(x);
                if (t != null) t
                else throw 'No such variable $x.';
            case RenameTable(e2, name):
                recurse(e2).renameTable(name);
            case JoinEq(left, right, leftColumn, rightColumn):
                recurse(left).joinEq(recurse(right), leftColumn, rightColumn);
            case JoinLeftEq(left, right, leftColumn, rightColumn):
                recurse(left).joinLeftEq(recurse(right), leftColumn, rightColumn);
            case JoinRightEq(left, right, leftColumn, rightColumn):
                recurse(left).joinRightEq(recurse(right), leftColumn, rightColumn);
            case JoinOuterEq(left, right, leftColumn, rightColumn):
                recurse(left).joinOuterEq(recurse(right), leftColumn, rightColumn);
            case Product(left, right):
                recurse(left).product(recurse(right));
            case Union(left, right):
                recurse(left).union(recurse(right));
            case SelectColumns(e2, columns):
                recurse(e2).selectColumns(columns);
            case RenameColumn(e2, from, to):
                recurse(e2).renameColumn(from, to);
            case Filter(e2, p):
                recurse(e2).filter(p, externals);
            case WithColumn(e2, name, e):
                recurse(e2).withColumn(name, e, externals);
            case Distinct(e2, e):
                recurse(e2).distinct(e, externals);
            case Map(e2, cs):
                recurse(e2).map(cs, externals);
            case GroupBy(e2, e):
                throw "eval: GroupBy";
            case Aggregate(e2, cs):
                evalGrouped(e2, env, externals).aggregate(cs, externals);
            case AggregateAll(e2, cs):
                recurse(e2).aggregateAll(cs, externals);
            case Sort(e2, key, asc):
                recurse(e2).sort(key, asc, externals);
        }
    }

    static public function evalGrouped(
        e : TableExp, 
        env : Env<String, Table>,
        externals : String->Null<Array<String>->String>
    ) : GroupedTable {
        return switch (e) {
            case GroupBy(e2, groups):
                eval(e2, env, externals).groupBy(groups, externals);
            default:
                var t = eval(e, env, externals);
                {
                    name: t.name,
                    proof: t.proof, 
                    columns: t.columns,
                    headerColumns: [],
                    groups: [{
                        header: [],
                        data: t.data
                    }]
                };
        }
    }

}

