package stupidsql;

using stringskip.ParserUtils;
import stupidsql.Table;
using stupidsql.TableExp;
using stupidsql.ValueExp;
import stupidsql.detail.ParseSqlQuery;

enum SqlQuery {
    Select(select : SqlSelect);
    Union(left : SqlQuery, right : SqlQuery);
}

typedef SqlSelect = {
    columns : ValueExp, 
    from : SqlTable, 
    where : Null<ValueExp>, 
    group : Null<ValueExp>,
    orderBy : Null<{key : ValueExp, asc : Bool}>
}

enum SqlTable {
    Table(name : String);
    Alias(t : SqlTable, name : String);
    Select(select : SqlSelect);
    Join(t1 : SqlTable, t2 : SqlTable, on : ValueExp);
    LeftJoin(t1 : SqlTable, t2 : SqlTable, on : ValueExp);
    RightJoin(t1 : SqlTable, t2 : SqlTable, on : ValueExp);
    OuterJoin(t1 : SqlTable, t2 : SqlTable, on : ValueExp);
}

class SqlQueryUtils {

    static public function toQuery(s : String, name : String = "query") : Null<SqlQuery> {
        return s.doParseTraceNull(name, ParseSqlQuery.parseQuery.trimHxSpace());
    }
 
    static public function toOps(q : SqlQuery) : TableExp {
        return switch (q) {
            case Select(select): selectToOps(select);
            case Union(left, right): Union(toOps(left), toOps(right));
        }
    }

    static private function selectToOps(select : SqlSelect) : TableExp {
        var source = tableToOps(select.from);
        //todo: filter on calculated columns?
        var filtered 
          = if (select.where != null) Filter(source, select.where)
            else source;
        var ordered
          = if (select.orderBy != null) Sort(filtered, select.orderBy.key, select.orderBy.asc)
            else filtered;
        //TODO: Improve distinct transformation.
        var calculated 
          = if (select.group != null) switch (select.columns) {
                case Count(Distinct(e)):
                    Aggregate(Distinct(GroupBy(ordered, select.group), e), Count(All));
                case As(Count(Distinct(e)), name):
                    Aggregate(Distinct(GroupBy(ordered, select.group), e), As(Count(All), name));
                default:
                    Aggregate(GroupBy(ordered, select.group), select.columns);
            } else switch (select.columns) {
                case Distinct(e): 
                    Distinct(ordered, e);
                case As(Distinct(e), name): 
                    Distinct(ordered, As(e, name));
                case Count(Distinct(e)): 
                    AggregateAll(Distinct(ordered, e), Count(All));
                case As(Count(Distinct(e)), name): 
                    AggregateAll(Distinct(ordered, e), As(Count(All), name));
                default: 
                    if (select.columns.isAggregate()) 
                        AggregateAll(ordered, select.columns)
                    else
                        Map(ordered, select.columns);
            }
        return calculated;
}

    static private function tableToOps(t : SqlTable) : TableExp {
        return switch (t) {
            case Table(n): Var(n);
            case Alias(t2, n): RenameTable(tableToOps(t2), n);
            case Join(t1, t2, Eq(Column(c1), Column(c2))): 
                JoinEq(tableToOps(t1), tableToOps(t2), c1, c2);
            case Join(t1, t2, on):
                Filter(Product(tableToOps(t1), tableToOps(t2)), on);
            case LeftJoin(t1, t2, Eq(Column(c1), Column(c2))): 
                JoinLeftEq(tableToOps(t1), tableToOps(t2), c1, c2);
            case LeftJoin(t1, t2, on):
                throw "not implemented";
            case RightJoin(t1, t2, Eq(Column(c1), Column(c2))): 
                JoinRightEq(tableToOps(t1), tableToOps(t2), c1, c2);
            case RightJoin(t1, t2, on):
                throw "not implemented";
            case OuterJoin(t1, t2, Eq(Column(c1), Column(c2))):
                JoinOuterEq(tableToOps(t1), tableToOps(t2), c1, c2);
            case OuterJoin(t1, t2, on):
                throw "not implemented";
            case Select(select): selectToOps(select);
        }
    }

}

