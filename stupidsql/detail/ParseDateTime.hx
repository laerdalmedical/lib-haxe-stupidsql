package stupidsql.detail;

using stringskip.Parse;
using stringskip.Skip;
using stringskip.StringIndex;
using stupidsql.DateTime;
using stupidsql.detail.ParseDateTime;

class ParseDateTime {

    static public function parseDateTime(i : StringIndex) : DateTime {
        var date = i.parseDate();
        i.skipSpace();
        var time = if (i.indicateTime()) i.parseTime()
        else null;
        return {date: date, time: time};
    }
    static public function parseDateTimeEnUS(i : StringIndex) : DateTime {
        var date = i.parseDateEnUS();
        i.skipSpace();
        var time = if (i.indicateTime()) i.parseTime()
        else null;
        return {date: date, time: time};

    }

    static public function parseDate(i : StringIndex) : Date {
        var year = i.parseInt();
        i.skipTheChars("-");
        var month = i.parseInt();
        i.skipTheChars("-");
        var day = i.parseInt();
        return {year: year, month: month, day: day};
    }
    static public function indicateDate(i : StringIndex) : Bool {
        var i2 = i.clone();
        return i2.tryDigits()
            && i2.tryTheChars("-");
    }

    static public function parseDateEnUS(i : StringIndex) : Date {
        var month = i.parseInt();
        i.skipTheChars("/");
        var day = i.parseInt();
        i.skipTheChars("/");
        var year = i.parseInt();
        return {year: year, month: month, day: day};
    }

    static public function parseTime(i : StringIndex) : Time {
        var hour = i.parseInt();
        i.skipTheChars(":");
        var minute = i.parseInt();
        i.skipTheChars(":");
        var second = i.parseInt();
        return {hour: hour, minute: minute, second: second};
    }
    static public function indicateTime(i : StringIndex) : Bool {
        var i2 = i.clone();
        return i2.tryDigits()
            && i2.tryTheChars(":");
    }
}