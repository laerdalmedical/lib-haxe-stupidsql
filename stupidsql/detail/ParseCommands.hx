package stupidsql.detail;

using StringTools;
using stringskip.Parse;
using stringskip.ParseHxString;
using stringskip.Skip;
using stringskip.StringIndex;
using stupidsql.SqlQuery;
import stupidsql.Commands;
import stupidsql.Table;
using stupidsql.detail.ParseSqlQuery;

class ParseCommands {
    static public function parseCommands(i : StringIndex) : Commands {
        var cs = [];
        while (i.indicateName()) {
            cs.push(parseCommand(i));
            i.skipHxSpace();
        }
        return cs;
    }

    static public function parseCommand(i : StringIndex) : Command {
        return if (i.tryTheName("print")) {
            i.skipHxSpace();
            var e = parseExp(i);
            i.skipHxSpace();
            i.skipTheChars(";");
            Print(e);
        
        } else if (i.tryTheName("save")) {
            i.skipHxSpace();
            var name = i.parseHxStringUtf8();
            i.skipHxSpace();
            var e = parseExp(i);
            i.skipHxSpace();
            i.skipTheChars(";");
            Save(name, e);

        } else if (indicateSet(i)) {
            var x = i.parseName();
            i.skipHxSpace();
            i.skipTheChars("=");
            i.skipHxSpace();
            var e = parseExp(i);
            i.skipHxSpace();
            i.skipTheChars(";");
            Set(x, e);

        } else if (i.indicateName()) {
            var e = parseExp(i);
            i.skipHxSpace();
            i.skipTheChars(";");
            Do(e);
            
        } else {
            i.expected("a command.");
            i.skipToSane();
            return Do(Csv("?", ";".code));
        }
    }

    static private function indicateSet(i : StringIndex) : Bool {
        var i2 = i.clone();
        return i2.tryName()
            && i2.tryHxSpace()
            && i2.tryTheChars("=");
    }

    static private function parseExp(i : StringIndex) : Exp {
        return if (i.tryTheName("csv")) {
            i.skipHxSpace();
            i.skipTheChars("(");
            i.skipHxSpace();
            var name = i.parseHxStringUtf8();
            i.skipHxSpace();
            var sep = if (i.tryTheChars(",")) {
                i.skipHxSpace();
                var iSep = i.clone();
                var sep = i.parseHxStringUtf8();
                i.skipHxSpace();
                if (sep.length == 1) sep.fastCodeAt(0);
                else {
                    iSep.expected("a singleton string");
                    null;
                }
            } else null;
            i.skipTheChars(")");
            Csv(name, null);
        } else if (i.tryTheName("tsv")) {
            i.skipHxSpace();
            i.skipTheChars("(");
            i.skipHxSpace();
            var name = i.parseHxStringUtf8();
            i.skipHxSpace();
            i.skipTheChars(")");
            Csv(name, "\t".code);
        } else if (i.indicateTheNameCaseless("SELECT")) {
            var q = i.parseQuery();
            Sql(q.toOps());
        } else {
            var x = i.parseName();
            Var(x);
        }
    }
}