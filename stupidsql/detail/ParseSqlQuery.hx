package stupidsql.detail;

using StringTools;
using simpleutil.CharCategories;
using stringskip.Parse;
using stringskip.ParseHxString;
using stringskip.Skip;
using stringskip.StringIndex;
import stupidsql.SqlQuery;
import stupidsql.Table;
using stupidsql.ValueExp;

class ParseSqlQuery {
    static public function parseQuery(i : StringIndex) : SqlQuery {
        var query = Select(parseSelect(i));
        i.skipHxSpace();
        while (i.tryTheNameCaseless("UNION")) {
            i.skipHxSpace();
            var query1 = Select(parseSelect(i));
            i.skipHxSpace();
            query = Union(query, query1);
        }
        return query;
    }
    static private function parseSelect(i : StringIndex) : SqlSelect {
        i.skipTheNameCaseless("SELECT");
        i.skipHxSpace();
        var columns = parseExp(i);
        i.skipHxSpace();
        i.skipTheNameCaseless("FROM");
        i.skipHxSpace();
        var from = parseTable(i);
        i.skipHxSpace();
        var where = if (i.tryTheNameCaseless("WHERE")) {
            i.skipHxSpace();
            var e = parseExp(i);
            i.skipHxSpace();
            e;
        } else null;
        var group = if (i.tryTheNameCaseless("GROUP")) {
            i.skipHxSpace();
            i.skipTheNameCaseless("BY");
            i.skipHxSpace();
            var e = parseExp(i);
            i.skipHxSpace();
            e;
        } else null;
        var orderBy = if (i.tryTheNameCaseless("ORDER")) {
            i.skipHxSpace();
            i.skipTheNameCaseless("BY");
            i.skipHxSpace();
            var e = parseExp(i);
            i.skipHxSpace();
            var asc 
              = if (i.tryTheNameCaseless("ASC")) true
                else if (i.tryTheNameCaseless("DESC")) false
                else true;
            {key: e, asc: asc};
        } else null;
        return {
            columns: columns, 
            from: from, 
            where: where, 
            group: group,
            orderBy: orderBy
        };
    }

    static private function parseTable(i : StringIndex) : SqlTable {
        var t = parseTableAs(i);
        i.skipHxSpace();
        while (i.indicateTheNameCaseless("JOIN") || i.indicateTheNameCaseless("INNER")
            || i.indicateTheNameCaseless("LEFT") || i.indicateTheNameCaseless("RIGHT")
            || i.indicateTheNameCaseless("FULL")) {
            if (i.tryTheNameCaseless("JOIN")) {
                i.skipHxSpace();
                var t2 = parseTableAs(i);
                i.skipHxSpace();
                i.skipTheNameCaseless("ON");
                i.skipHxSpace();
                var on = parseExp(i);
                t = Join(t, t2, on);
            } else if (i.tryTheNameCaseless("INNER")) {
                i.skipHxSpace();
                i.skipTheNameCaseless("JOIN");
                i.skipHxSpace();
                var t2 = parseTableAs(i);
                i.skipHxSpace();
                i.skipTheNameCaseless("ON");
                i.skipHxSpace();
                var on = parseExp(i);
                t = Join(t, t2, on);
            } else if (i.tryTheNameCaseless("LEFT")) {
                i.skipHxSpace();
                i.skipTheNameCaseless("JOIN");
                i.skipHxSpace();
                var t2 = parseTableAs(i);
                i.skipHxSpace();
                i.skipTheNameCaseless("ON");
                i.skipHxSpace();
                var on = parseExp(i);
                t = LeftJoin(t, t2, on);
            } else if (i.tryTheNameCaseless("RIGHT")) {
                i.skipHxSpace();
                i.skipTheNameCaseless("JOIN");
                i.skipHxSpace();
                var t2 = parseTableAs(i);
                i.skipHxSpace();
                i.skipTheNameCaseless("ON");
                i.skipHxSpace();
                var on = parseExp(i);
                t = RightJoin(t, t2, on);
            } else if (i.tryTheNameCaseless("FULL")) {
                i.skipHxSpace();
                i.skipTheNameCaseless("OUTER");
                i.skipHxSpace();
                i.skipTheNameCaseless("JOIN");
                i.skipHxSpace();
                var t2 = parseTableAs(i);
                i.skipHxSpace();
                i.skipTheNameCaseless("ON");
                i.skipHxSpace();
                var on = parseExp(i);
                t = OuterJoin(t, t2, on);
                
            } else {
                throw "weird";
            }
            i.skipHxSpace();
        }
        return t;
    }
    static private function parseTableAs(i : StringIndex) : SqlTable {
        var t = parseTableSimple(i);
        i.skipHxSpace();
        return if (i.tryTheNameCaseless("AS")) {
            i.skipHxSpace();
            var alias = i.parseName();
            Alias(t, alias);
        } else t;
    }
    static private function parseTableSimple(i : StringIndex) : SqlTable {
        return if (i.tryTheChars("(")) {
            i.skipHxSpace();
            var t = parseTable(i);
            i.skipHxSpace();
            i.skipTheChars(")");
            t;
        } else if (i.indicateTheNameCaseless("SELECT")) {
            Select(parseSelect(i));
        } else if (i.indicateName()) {
            var name = parseName(i);
            Table(name);
        } else {
            i.expected("'(', 'SELECT', or a table name");
            i.skipToSane();
            Table("?");
        }
    }

    static private function parseExp(i : StringIndex) : ValueExp {
        return parseExpRow(i);
    }
    static private function parseExpRow(i : StringIndex) : ValueExp {
        var e = parseExpAs(i);
        i.skipHxSpace();
        return if (i.indicateTheChars(",")) {
            var es = [e];
            while (i.tryTheChars(",")) {
                i.skipHxSpace();
                es.push(parseExpAs(i));
                i.skipHxSpace();
            }
            Row(es);
        } else e;
    }
    static private function parseExpAs(i : StringIndex) : ValueExp {
        var e = parseExpOr(i);
        i.skipHxSpace();
        return if (i.tryTheNameCaseless("AS")) {
            i.skipHxSpace();
            var name = parseName(i);
            As(e, {ns: null, name: name});
        } else e;
    }
    static private function parseExpOr(i : StringIndex) : ValueExp {
        var e = parseExpAnd(i);
        i.skipHxSpace();
        while (i.tryTheNameCaseless("OR")) {
            i.skipHxSpace();
            var e1 = parseExpAnd(i);
            i.skipHxSpace();
            e = Or(e, e1);
        }
        return e;
    }
    static private function parseExpAnd(i : StringIndex) : ValueExp {
        var e = parseExpEq(i);
        i.skipHxSpace();
        while (i.tryTheNameCaseless("AND")) {
            i.skipHxSpace();
            var e1 = parseExpEq(i);
            i.skipHxSpace();
            e = And(e, e1);
        }
        return e;
    }
    static private function parseExpEq(i : StringIndex) : ValueExp {
        function transformLike(e1 : ValueExp, e2 : ValueExp, iPattern : StringIndex) : ValueExp {
            return switch (e2) {
                case Literal(p):
                    if (p.length > 0 && p.fastCodeAt(0) == "%".code) {
                        var postfix = p.substr(1);
                        if (p.length > 0 && p.fastCodeAt(p.length - 1) == "%".code) {
                            var substring = postfix.substr(0, postfix.length - 1);
                            if (substring.indexOf("%") >= 0 || substring.indexOf("_") >= 0) {
                                iPattern.semantic("Unsupported pattern.");
                            }
                            StringContains(e1, substring);
                        } else {
                            if (postfix.indexOf("%") >= 0 || postfix.indexOf("_") >= 0) {
                                iPattern.semantic("Unsupported pattern.");
                            }
                            StringPostfix(e1, postfix);
                        }
                    } else if (p.length > 0 && p.fastCodeAt(p.length - 1) == "%".code) {
                        var prefix = p.substr(0, p.length - 1);
                        if (prefix.indexOf("%") >= 0 || prefix.indexOf("_") >= 0) {
                            iPattern.semantic("Unsupported pattern.");
                        }
                        StringPrefix(e1, prefix);
                    } else {
                        Eq(e1, e2);
                    }
                default:
                    iPattern.semantic("Unsupported pattern.");
                    Eq(e1, e2);
            }

        }
        var e = parseExpSimple(i);
        i.skipHxSpace();
        if (i.indicateTheChars("=") || i.indicateTheChars("!=") 
         || i.indicateTheChars("<=") || i.indicateTheChars("<") 
         || i.indicateTheChars(">=") || i.indicateTheChars(">") 
         || i.indicateTheNameCaseless("LIKE") || i.indicateTheNameCaseless("NOT")) {
            if (i.tryTheChars("=")) {
                i.skipHxSpace();
                var e2 = parseExpSimple(i);
                e = Eq(e, e2);
            } else if (i.tryTheChars("!=")) {
                i.skipHxSpace();
                var e2 = parseExpSimple(i);
                e = Not(Eq(e, e2));
            } else if (i.tryTheChars("<=")) {
                i.skipHxSpace();
                var e2 = parseExpSimple(i);
                e = Not(Lt(e2, e));
            } else if (i.tryTheChars("<")) {
                i.skipHxSpace();
                var e2 = parseExpSimple(i);
                e = Lt(e, e2);
            } else if (i.tryTheChars(">=")) {
                i.skipHxSpace();
                var e2 = parseExpSimple(i);
                e = Not(Lt(e, e2));
            } else if (i.tryTheChars(">")) {
                i.skipHxSpace();
                var e2 = parseExpSimple(i);
                e = Lt(e2, e);
            } else if (i.tryTheNameCaseless("LIKE")) {
                i.skipHxSpace();
                var iPattern = i.clone();
                var e2 = parseExpSimple(i);
                e = transformLike(e, e2, iPattern);
            } else if (i.tryTheNameCaseless("NOT")) {
                i.skipHxSpace();
                i.skipTheNameCaseless("LIKE");
                i.skipHxSpace();
                var iPattern = i.clone();
                var e2 = parseExpSimple(i);
                e = Not(transformLike(e, e2, iPattern));
            } else {
                throw "weird";
            }
            i.skipHxSpace();
        }
        return e;        
    }
    static private function parseExpSimple(i : StringIndex) : ValueExp {
        return if (i.tryTheChars("(")) {
            i.skipHxSpace();
            var e = parseExp(i);
            i.skipHxSpace();
            i.skipTheChars(")");
            e;
        } else if (i.tryTheChars("*")) {
            All;
        } else if (i.indicateTheChars("'")) {
            Literal(i.parseHxStringUtf8());
        } else if (i.tryTheChars('«')) {
            i.skipHxSpace();
            var e = parseExpExternal(i);
            i.skipHxSpace();
            i.skipTheChars("»");
            e;
        } else if (i.tryTheNameCaseless("CASE")) {
            i.skipHxSpace();
            var cs = [];
            while (i.has() && !i.indicateTheNameCaseless("END") && !i.indicateTheNameCaseless("ELSE")) {
                var iCase = i.clone();
                if (i.tryTheNameCaseless("WHEN")) {
                    i.skipHxSpace();
                    var cond = parseExp(i);
                    i.skipHxSpace();
                    i.skipTheNameCaseless("THEN");
                    i.skipHxSpace();
                    var value = parseExp(i);
                    cs.push({cond: cond, value: value});
                } else {
                    i.expected("'WHEN', 'ELSE', or 'END'");
                    i.skipToSane();
                }
            }
            var fallback = if (i.tryTheNameCaseless("ELSE")) {
                i.skipHxSpace();
                var e = parseExp(i);
                i.skipHxSpace();
                e;
            } else Literal("null");
            i.skipTheNameCaseless("END");
            Case(cs, fallback);
        } else if (i.tryTheNameCaseless("IF")) {
            i.skipHxSpace();
            i.skipTheChars("(");
            i.skipHxSpace();
            var cond = parseExpAs(i);
            i.skipHxSpace();
            i.skipTheChars(",");
            i.skipHxSpace();
            var onTrue = parseExpAs(i);
            i.skipHxSpace();
            i.skipTheChars(",");
            i.skipHxSpace();
            var onFalse = parseExpAs(i);
            i.skipHxSpace();
            i.skipTheChars(")");
            Case([{cond: cond, value: onTrue}], onFalse);
        } else if (i.tryTheNameCaseless("NOT")) {
            i.skipHxSpace();
            var e2 = parseExpSimple(i);
            Not(e2);
        } else if (i.tryTheNameCaseless("DISTINCT")) {
            i.skipHxSpace();
            var e2 = parseExpSimple(i);
            Distinct(e2);
        } else if (i.tryTheNameCaseless("EXTRACT")) {
            i.skipHxSpace();
            i.skipTheChars("(");
            i.skipHxSpace();
            var iPart = i.clone();
            var partString = i.parseName();
            i.skipHxSpace();
            i.skipTheChars(",");
            i.skipHxSpace();
            var e = parseExp(i);
            i.skipHxSpace();
            i.skipTheChars(")");
            var part = switch (partString.toLowerCase()) {
                case "year": Year;
                case "month": Month;
                case "day": Day;
                case "hour": Hour;
                case "minute": Minute;
                case "second": Second;
                case "quarter": Quarter;
                default:
                    iPart.semantic("Unrecognized date-time part.");
                    Year;
            }
            Extract(part, e);
        } else if (i.tryTheNameCaseless("PARSE")) {
            i.skipHxSpace();
            i.skipTheChars("(");
            i.skipHxSpace();
            var s = parseExpSimple(i);
            i.skipHxSpace();
            i.skipTheNameCaseless("AS");
            i.skipHxSpace();
            var iType = i.clone();
            var type = switch (i.parseName()) {
                case "date": Date;
                default:
                    i.expected("a type");
                    Date;
            }
            i.skipHxSpace();
            var culture = if (i.tryTheNameCaseless("USING")) {
                i.skipHxSpace();
                var c = parseIetfLanguageTag(i);
                i.skipHxSpace();
                c;
            } else null;
            i.skipTheChars(")");
            Parse(s, type, culture);
        } else if (indicateFunCall(i)) {
            var iFun = i.clone();
            var f = parseName(i);
            i.skipHxSpace();
            i.skipTheChars("(");
            i.skipHxSpace();
            var as = parseExp(i);
            i.skipHxSpace();
            i.skipTheChars(")");
            switch (f.toUpperCase()) {
                case "COUNT": Count(as);
                default: 
                    iFun.semantic('Undefined function $f.');
                    Literal("?");
            }
        } else if (indicateName(i)) {
            Column(parseColumnName(i));
        } else {
            i.expected("'(', a name, or a string");
            i.skipToSane();
            Literal("?");
        }
    }
    static private function parseExpExternal(i : StringIndex) : ValueExp {
        return if (i.tryTheChars("›")) {
            i.skipHxSpace();
            var e = parseExp(i);
            i.skipHxSpace();
            i.skipTheChars("‹");
            e;
        } else if (i.indicateTheChars("'")) {
            var s = i.parseHxStringUtf8();
            Literal(s);
        } else if (indicateName(i)) {
            var x = parseName(i);
            i.skipHxSpace();
            if (i.indicateTheChars("(")) {
                var as = i.parseDelimitedListHx("(", ")", ",", parseExpExternal);
                Op(x, as);
            } else if (i.tryTheChars(".")) {
                i.skipHxSpace();
                var y = parseName(i);
                Column({ns: x, name: y});
            } else Column({ns: null, name: x});
        } else {
            i.expected("an external expression");
            i.skipToSane();
            Literal("?");
        }
    }

    static private function indicateFunCall(i : StringIndex) : Bool {
        var i2 = i.clone();
        return i2.tryName()
            && i2.tryHxSpace()
            && i2.tryTheChars("(");
    }

    static private function parseColumnName(i : StringIndex) : ColumnName {
        var part1 = parseName(i);
        return if (i.tryTheChars(".")) {
            var part2 = parseName(i);
            {ns: part1, name: part2};
        } else {ns: null, name: part1};
    }
    static private function indicateName(i : StringIndex) : Bool {
        return i.indicateName() || i.indicateTheChars("\"");
    }
    static private function parseName(i : StringIndex) : String {
        return if (i.indicateTheChars("\"")) i.parseHxStringUtf8()
        else if (i.indicateName()) i.parseName()
        else {
            i.expected("a name");
            i.skipToSane();
            "?";
        }
    }
    static private function parseIetfLanguageTag(i : StringIndex) : Culture {
        var i0 = i.clone();
        while (i.has() && (i.get() == "-".code || i.get().isAlphanum())) {
            i.skip();
        }
        return switch (i.getRange(i0)) {
            case "en-US": EnUS;
            default:
                i0.expected("an IETF language tag");
                EnUS;
        }
    }
}