package stupidsql.detail;

using simpleutil.Annotate;
using simpleutil.CharCategories;
using simpleutil.KeyValue;
using stringskip.Parse;
using stringskip.ParseHxString;
using stringskip.Skip;
using stringskip.StringIndex;
using stringskip.StringPos;
using stupidsql.TableExp;
using stupidsql.ValueExp;

class ParseTableExp {
    static public function parseValueExp(i : StringIndex) : ValueExp {}

    static private function parseValueExpSimple(i : StringIndex) : ValueExp {
        var es = parseValueExpSimples(i);
        return if (es.length == 1) es[0]
        else {
            i.expected("a single expression");
            Column(["?"]);
        }
    }

    static private function parseValueExpSimples(i : StringIndex) : Array<ValueExp> {
        return if (i.indicateTheChars("(")) {
            i.parseDelimitedListHx("(", ")", ",", parseValueExp);
        } else if (i.tryTheChars("*")) {
            [All];
        } else if (i.tryTheChars("!")) {
            i.skipHxSpace();
            var e = parseValueExpSimple(i);
            [Not(e)];
        } else if (i.indicateHxString()) {
            var s = i.parseHxStringUtf8();
            [Literal(s)];
        } else if (i.indicateFloat(false)) {
            var i0 = i.clone();
            i.skipFloat(false);
            var s = i0.range(i);
            [Literal(s)];
        } else if (i.indicateInt()) {
            var i0 = i.clone();
            i.skipInt();
            var s = i0.range(i);
            [Literal(s)];
        } else if (i.indicateName()) {
            var x = i.parseName();
            switch (x) {
                case "if":
                    var cs = [];
                    function one() {
                        i.skipHxSpace();
                        var cond = parseValueExp(i);
                        i.skipHxSpace();
                        i.skipTheChars("then");
                        i.skipHxSpace();
                        var onTrue = parseValueExp(i);
                        i.skipHxSpace();
                        i.skipTheChars("else");
                        i.skipHxSpace();
                        cs.push({cond: cond, value: onTrue});        
                    }
                    one();
                    while (i.tryTheName("if")) {
                        one();
                    }
                    var fallback = i.parseValueExpSimple(i);
                    Cond(cs, fallback);
                case ""
        
            }
        }

    }

    

}