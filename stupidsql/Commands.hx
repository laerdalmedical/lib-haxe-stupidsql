package stupidsql;

using simpleutil.Env;
using simpleutil.Process2;
using stringskip.ParserUtils;
import filesystem2.IRTextFs;
import filesystem2.IWTextFs;
using stupidsql.Ops;
using stupidsql.TableExp;
using stupidsql.Table;
import stupidsql.detail.ParseCommands;

typedef Commands = Array<Command>;
enum Command {
    Do(e : Exp);
    Set(x : String, e : Exp);
    Print(e : Exp);
    Save(name : String, e : Exp);
}

enum Exp {
    Var(x : String);
    Csv(name : String, sep : Null<Int>);
    Sql(q : TableExp);
    External(id : String, as : Array<Exp>);
}


class CommandsUtils {
    static public function toCommands(s : String, name : String) : Null<Commands> {
        return s.doParseTraceNull(name, ParseCommands.parseCommands.trimHxSpace());
    }
    static public function runCommands(
        cs : Commands, 
        print : Table->Process2<{}>, 
        rfs : IRTextFs, wfs : IWTextFs, 
        env : Null<Env<String, Table>>,
        externals : String->Null<Array<Table>->Process2<Table>>,
        externalOps : String->Null<Array<String>->String>
    ) : Process2<{}> {
        env = if (env != null) env
              else EnvUtils.empty();
        function run(i : Int, env : Env<String, Table>) : Process2<{}> {
            function eval(e : Exp) : Process2<Table> {
                return evalExp(e, env, externals, externalOps, rfs);
            }
            return if (i < cs.length) switch (cs[i]) {
                case Set(x, e):
                    eval(e).mbind(function(t) return {
                    var t2 = t.renameTable(x);
                    run(i + 1, env.set(x, t2));
                    });
                case Do(e):
                    eval(e).mmap(function(_) return {});
                case Print(e):
                    eval(e).mbind(function(t) return
                    print(t).mbind(function(_) return
                    run(i + 1, env)));
                case Save(name, e):
                    eval(e).mbind(function(t) return
                    wfs.writeFile(name, t.showCsv()).mbind(function(_) return 
                    run(i + 1, env)));
            } else ({}).resolved();
        }
        return run(0, env);
    }

    static private function evalExp(
        e : Exp, 
        env : Env<String, Table>,
        externals : String->Null<Array<Table>->Process2<Table>>,
        externalOps : String->Null<Array<String>->String>,
        fs : IRTextFs
    ) : Process2<Table> {
        function eval(e : Exp) : Process2<Table> {
            return evalExp(e, env, externals, externalOps, fs);
        }
        return switch (e) {
            case Csv(name, sep):
                fs.readFile(name).mmap(function(s) return 
                    if (sep != null) s.tableFromCsv(name, sep)
                    else s.tableFromCsvGuess(name)
                );
            case Sql(q):
                q.eval(env, externalOps).resolved();
            case Var(x):
                var t = env.tryGet(x);
                if (t != null) t.resolved()
                else 'Undefined variable $x'.failed();
            case External(id, as):
                var f = externals(id);
                if (f != null) {
                    var asp = as.map(eval).ands();
                    asp.mbind(f);
                } else 'External $id not defined.'.failed();
        }
    }
}