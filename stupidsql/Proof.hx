package stupidsql;

using Lambda;
using simpleutil.IteratorUtils;
using simpleutil.MapUtils;
using stupidsql.Value;

enum Proof {
    Value(v : Value);
    Node(op : String, as : Array<Proof>);
}


class ProofUtils {
    static public function stringProof(s : String) : Proof {
        return Value(String(s));
    }
    static public function listProof(ps : Array<Proof>) : Proof {
        return Node("[]", ps);
    }
    static public function recordProof(fs : Map<String, Proof>) : Proof {
        return Node("{}", 
            fs.pairs()
                .map((p)->Node(":", [
                    Value(String(p.key)),
                    p.value
                ]))
                .array()
        );
    }
    static public function quoteProof1(v : String) : Proof {
        return Node("[[]]", [stringProof(v)]);
    }
    static public function quoteProof2(ns : String, v : String) : Proof {
        return Node("[[]]", [stringProof(ns), stringProof(v)]);
    }
    static public function show(p : Proof) : String {
        return switch (p) {
            case Value(v): v.show();
            case Node(":", [l, r]):
                show(l) + ": " + show(r);
            case Node("{}", as): 
                "{" + as.showElements(show, ", ") + "}";
            case Node("[]", as): 
                "[" + as.showElements(show, ", ") + "]";
            case Node("[[]]", [Value(String(v))]):
                "⟦" + v + "⟧";
            case Node("[[]]", [Value(String(ns)), Value(String(v))]):
                ns + "⟦" + v + "⟧";
            case Node(op, as): 
                if (as.length > 0) switch (as[0]) {
                    case Node(_, _): 
                        '${show(as[0])}.$op(${as.drop(1).showElements(show, ", ")})';
                    default: 
                        op + "(" + as.showElements(show, ", ") + ")";
                } else op;
                
        }
    }
}