package stupidsql;

using Lambda;
using simpleutil.ArrayUtils;
using simpleutil.HxEscape;
using simpleutil.IteratorUtils;
using simpleutil.MapUtils;
using simpleutil.MultimapUtils;
using stupidsql.ArrayUtils;
using stupidsql.ColumnName;
using stupidsql.Proof;
using stupidsql.Table;
using stupidsql.TableExp;
using stupidsql.ValueExp;

class Ops {

    // Renames a table.
    static public function renameTable(
        t : Table,
        name : String
    ) : Table {
        return {
            name: name,
            proof: Node("renameTable", [t.proof, name.stringProof()]),
            columns: t.columns.renameNss(name),
            data: t.data
        };
    }

    // Join (inner) of two tables on column identity.
    //TODO: generalize to row function instead of column.
    static public function joinEq(
        left : Table, 
        right : Table, 
        leftColumn : ColumnName, rightColumn : ColumnName
    ) : Table {
        var iLeft = left.columns.indexOfColumn(leftColumn);
        var iRight = right.columns.indexOfColumn(rightColumn);
        if (iLeft < 0 && iRight < 0) {
            iLeft = left.columns.indexOfColumn(rightColumn);
            iRight = right.columns.indexOfColumn(leftColumn);
        }
        if (iLeft >= 0 && iRight >= 0) {
            var rightIndex = new Map();
            for (row in right.data) {
                if (iRight < row.length) {
                    var key = row[iRight];
                    rightIndex.insertMultimap(key, row);
                }
            }
            var result = [];
            for (row in left.data) {
                if (iLeft < row.length) {
                    var key = row[iLeft];
                    var rightRows = rightIndex.getMultimap(key);
                    for (rightRow in rightRows) {
                        result.push(row.concat(rightRow));
                    }
                }
            }
            var columns = left.columns.concat(right.columns);
            return {
                name: 'join',
                proof: Node("joinEq", [left.proof, leftColumn.show().stringProof(), right.proof, rightColumn.show().stringProof()]),
                columns: columns,
                data: result
            };
        } else {
            if (iLeft < 0) {
                throw 'joinEq: No such column ${leftColumn.show()}.';
            } else {
                throw 'joinEq: No such column ${rightColumn.show()}.';
            }
        }
    }

    static public function joinOuterEq(
        left : Table,
        right : Table,
        leftColumn : ColumnName, rightColumn : ColumnName
    ) : Table {
        var iLeft = left.columns.indexOfColumn(leftColumn);
        var iRight = right.columns.indexOfColumn(rightColumn);
        if (iLeft >= 0 && iRight >= 0) {
            var rightIndex = new Map();
            for (row in right.data) {
                if (iRight < row.length) {
                    var key = row[iRight];
                    rightIndex.insertMultimap(key, row);
                }
            }
            var leftIndex = new Map();
            for (row in left.data) {
                if (iLeft < row.length) {
                    var key = row[iLeft];
                    leftIndex.insertMultimap(key, row);
                }
            }
            var result = [];
            for (row in left.data) {
                if (iLeft < row.length) {
                    var key = row[iLeft];
                    var rightRows = rightIndex.getMultimap(key);
                    if (rightRows.length > 0) {
                        for (rightRow in rightRows) {
                            result.push(row.concat(rightRow));
                        }
                    } else {
                        result.push(
                            row.concat("null".repeated(right.columns.length).array())
                        );
                    }
                }
            }
            for (row in right.data) {
                if (iRight < row.length) {
                    var key = row[iRight];
                    var leftRows = leftIndex.getMultimap(key);
                    if (leftRows.length == 0) {
                        result.push(
                            "null".repeated(left.columns.length).array().concat(row)
                        );
                    }
                }
            }
            var columns = left.columns.concat(right.columns);
            return {
                name : "join",
                proof: Node("joinOuterEq", [left.proof, leftColumn.show().stringProof(), right.proof, rightColumn.show().stringProof()]),
                columns: columns,
                data: result
            };
        } else {
            throw 'joinOuterEq: No such columns $leftColumn or $rightColumn.';
        }
    }

    // Left join of two tables on column identity.
    static public function joinLeftEq(
        left : Table, 
        right : Table, 
        leftColumn : ColumnName, rightColumn : ColumnName
    ) : Table {
        return joinLeftEqWorker(
            left, right, leftColumn, rightColumn, "joinLeftEq",
            function(row1, row2) return row1.concat(row2),
            function(row1, row2) return row1.concat(row2)
        );
    }

    // Right join of two tables on column identity.
    static public function joinRightEq(
        left : Table, 
        right : Table, 
        leftColumn : ColumnName, rightColumn : ColumnName
    ) : Table {
        return joinLeftEqWorker(
            right, left, rightColumn, leftColumn, "joinRightEq",
            function(row1, row2) return row2.concat(row1),
            function(row1, row2) return row2.concat(row1)
        );
    }

    static private function joinLeftEqWorker(
        left : Table, 
        right : Table, 
        leftColumn : ColumnName, rightColumn : ColumnName,
        opName : String,
        combineColumns : Array<ColumnName>->Array<ColumnName>->Array<ColumnName>,
        combine : Array<String>->Array<String>->Array<String>
    ) : Table {
        var iLeft = left.columns.indexOfColumn(leftColumn);
        var iRight = right.columns.indexOfColumn(rightColumn);
        if (iLeft >= 0 && iRight >= 0) {
            var rightIndex = new Map();
            for (row in right.data) {
                if (iRight < row.length) {
                    var key = row[iRight];
                    rightIndex.insertMultimap(key, row);
                }
            }
            var result = [];
            for (row in left.data) {
                if (iLeft < row.length) {
                    var key = row[iLeft];
                    var rightRows = rightIndex.getMultimap(key);
                    if (rightRows.length > 0) {
                        for (rightRow in rightRows) {
                            result.push(combine(row, rightRow));
                        }
                    } else {
                        result.push(combine(row, "null".repeated(right.columns.length).array()));
                    }
                }
            }
            var columns = combineColumns(left.columns, right.columns);
            return {
                name: "join",
                proof: Node(opName, [left.proof, leftColumn.show().stringProof(), right.proof, rightColumn.show().stringProof()]),
                columns: columns,
                data: result
            };
        } else {
            throw '$opName: No such columns $leftColumn or $rightColumn.';
        }    
    }


    // General product of two tables.
    static public function product(
        left : Table,
        right : Table
    ) : Table {
        var data = [];
        for (row1 in left.data) {
            for (row2 in right.data) {
                data.push(row1.concat(row2));
            }
        }
        return {
            name: "product",
            proof: Node("product", [left.proof, right.proof]),
            columns: left.columns.concat(right.columns),
            data: data
        };
    }

    static public function union(
        left : Table,
        right : Table
    ) : Table {
        return if (left.columns.eqNames(right.columns)) {
            name: "union", 
            proof: Node("union", [left.proof, right.proof]),
            columns: left.columns,
            data: left.data.concat(right.data)
        } else {
            throw 'union: unmatching column names.';
        }
    }

    // Selects only the specified columns.
    static public function selectColumns(
        t : Table,
        columns : Array<ColumnName>
    ) : Table {
        var iColumns = [];
        for (column in columns) {
            var iColumn = t.indexOfColumn(column);
            if (iColumn >= 0) {
                iColumns.push(iColumn);
            } else {
                throw 'selectColumns: No such column ${column.show()}';
            }
        }
        var columns2 = [];
        for (iColumn in iColumns) {
            columns2.push(t.columns[iColumn]);
        }
        var data2 = [];
        for (row in t.data) {
            var row2 = [];
            for (iColumn in iColumns) {
                row2.push(
                    if (0 <= iColumn && iColumn < row.length) row[iColumn]
                    else ""
                );
            }
            data2.push(row2);
        }
        return {
            name: t.name,
            proof: Node("selectColumns", [t.proof, columns.map((c)->c.show().stringProof()).listProof()]),
            columns: columns2,
            data: data2
        };
    }

    // Renames a single column.
    static public function renameColumn(
        t : Table,
        from : ColumnName,
        to : ColumnName
    ) : Table {
        var iColumn = t.indexOfColumn(from);
        if (0 <= iColumn && iColumn < t.columns.length) {
            var columns2 = t.columns.setAt(iColumn, to);
            return {
                name: t.name,
                proof: Node("renameColumn", [from.show().stringProof(), to.show().stringProof()]),
                columns: columns2,
                data: t.data
            };
        } else {
            throw 'renameColumn: No such column ${from.show()}.';
        }
    }

    // Adds a column.
    static public function withColumn(
        t : Table,
        name : ColumnName,
        value : ValueExp,
        externals : String->Null<Array<String>->String>
    ) : Table {
        var columns2 = t.columns.concat([name]);
        var data2 = [];
        for (row in t.data) {
            var v = value.evalAsValue(t.columns, row, externals);
            data2.push(row.concat([v]));
        }
        return {
            name: t.name, 
            proof: Node("withColumn", [t.proof, name.show().stringProof(), value.show().quoteProof1()]),
            columns: columns2,
            data: data2
        };
    }

    static public function distinct(
        t : Table,
        value : ValueExp,
        externals : String->Null<Array<String>->String>
    ) : Table {
        var rows = new Map();
        for (row in t.data) {
            var row = value.evalAsRow(t.columns, row, externals);
            var rowString = row.showElements(HxEscape.hxEscape, ",");
            rows.set(rowString, row);
        }
        var data2 = [];
        for (row in rows) {
            data2.push(row);
        }
        return {
            name: t.name, 
            proof: Node("distinct", [t.proof, value.show().quoteProof1()]),
            columns: value.getColumnNames(t.columns),
            data: data2
        };
    }

    // Maps a row function over each row in the table.
    static public function map(
        t : Table,
        e : ValueExp,
        externals : String->Null<Array<String>->String>
    ) : Table {
        var columns2 = e.getColumnNames(t.columns);
        var data2 = [];
        for (row in t.data) {
            var row2 = e.evalAsRow(t.columns, row, externals);
            data2.push(row2);
        }
        return {
            name: t.name,
            proof: Node("map", [t.proof, e.show().quoteProof1()]),
            columns: columns2,
            data: data2
        };
    }

    // Groups rows.
    static public function groupBy(
        t : Table,
        groups : ValueExp,
        externals : String->Null<Array<String>->String>
    ) : GroupedTable {
        var headerColumns2 = groups.getColumnNames([]);
        var groupings = mapGroupBy(t, groups, externals);
        return {
            name: t.name,
            proof: Node("groupBy", [t.proof, groups.show().quoteProof1()]),
            columns: t.columns,
            headerColumns: headerColumns2,
            groups: groupings.theValues().array()
        };
    }

    static public function mapGroupBy(
        t : Table, 
        groups : ValueExp,
        externals : String->Null<Array<String>->String>
    ) : Map<String, Group> {
        var groupings = new Map();
        for (row in t.data) {
            var header = groups.evalAsRow(t.columns, row, externals);
            var headerString = header.showElements(HxEscape.hxEscape, ",");
            var grouping = groupings.get(headerString);
            if (grouping == null) {
                groupings.set(headerString, {
                    header: header,
                    data: [row]
                });
            } else {
                grouping.data.push(row);
            }
        }
        return groupings;
    }

    static public function groupName(
        columns : Array<ColumnName>,
        row : Array<String>,
        groups : ValueExp,
        externals : String->Null<Array<String>->String>
    ) : String {
        return groups
            .evalAsRow(columns, row, externals)
            .showElements(HxEscape.hxEscape, ",");
    }

    // Aggregate over all rows in the table.
    static public function aggregateAll(
        t : Table,
        e : ValueExp,
        externals : String->Null<Array<String>->String>
    ) : Table {
        var columns2 = e.getColumnNames(t.columns);
        var group = {
            header: [],
            data: t.data
        };
        var data2 = [e.evalAsAggregateRow(t.columns, [], group, externals)];
        return {
            name: t.name,
            proof: Node("aggregateAll", [t.proof, e.show().quoteProof1()]),
            columns: columns2,
            data: data2
        };
    }

    // Aggregate over all groups.
    static public function aggregate(
        t : GroupedTable,
        e : ValueExp,
        externals : String->Null<Array<String>->String>
    ) : Table {
        var columns2 = e.getColumnNames(t.headerColumns);
        var data2 = [];
        for (group in t.groups) {
            data2.push(e.evalAsAggregateRow(t.columns, t.headerColumns, group, externals));
        }
        return {
            name: t.name,
            proof: Node("aggregate", [t.proof, e.show().quoteProof1()]),
            columns: columns2,
            data: data2
        };
    }

    // Filters rows that satisfy `p`. 
    static public function filter(
        t : Table,
        p : ValueExp,
        externals : String->Null<Array<String>->String>
    ) : Table {
        var data2 = [];
        for (row in t.data) {
            if (p.evalAsValue(t.columns, row, externals).asBool()) {
                data2.push(row);
            }
        }
        return {
            name: t.name,
            proof: Node("filter", [t.proof, p.show().quoteProof1()]),
            columns: t.columns,
            data: data2
        };
    }

    // Sorts the row according to `e`.
    // Sorts ascending iff `asc`.
    static public function sort(
        t : Table,
        e : ValueExp,
        asc : Bool,
        externals : String->Null<Array<String>->String>
    ) : Table {
        var data2 = sortWorker(t.columns, t.data, e, asc, externals);
        return {
            name: t.name,
            proof: Node("sort", [t.proof, e.show().quoteProof1()]),
            columns: t.columns,
            data: data2
        };
    }

    // Sorts each group.
    static public function sortGrouped(
        t : GroupedTable,
        e : ValueExp,
        asc : Bool,
        externals : String->Null<Array<String>->String>
    ) : GroupedTable {
        var groups2 = t.groups.map(function(group) return {
            header: group.header,
            data: sortWorker(t.columns, group.data, e, asc, externals)
        });
        return {
            name: t.name,
            proof: Node("sortGrouped", [t.proof, e.show().quoteProof1()]),
            columns: t.columns,
            headerColumns: t.headerColumns,
            groups: groups2
        };
    }

    static private function sortWorker(
        columns : Array<ColumnName>,
        data : Array<Array<String>>,
        e : ValueExp,
        asc : Bool,
        externals : String->Null<Array<String>->String>
    ) : Array<Array<String>> {
        var keys = [];
        var types = [];
        function mergeType(t1 : Array<ValueType>, t2 : Array<ValueType>) : Array<ValueType> {
            return t1.zipSquashWithLong(t2, ValueExpUtils.lubType);
        }
        for (i in 0...data.length) {
            var row = data[i];
            var key = e.evalAsRow(columns, row, externals);
            var types2 = key.map(ValueExpUtils.typeOfValue);
            keys.push({row: row, key: key});
            types = mergeType(types, types2);
        }
        var orders = types.map(ValueExpUtils.orderTyped);
        function keyOrder(
            k1 : {key : Array<String>, row : Array<String>}, 
            k2 : {key : Array<String>, row : Array<String>}
        ) : Int {
            for (i in 0...types.length) {
                var o = orders[i](k1.key[i], k2.key[i]);
                if (o != 0) {
                    return if (asc) o else -o;
                }
            }
            return 0;
        }
        keys.sort(keyOrder);
        var data2 = keys.map(function(key) return key.row);
        return data2;
    }

}
